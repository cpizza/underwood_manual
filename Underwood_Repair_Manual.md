![](header_1.png)
![](header_2.png)
![](header_3.png)

# INTRODUCTION

The only way to make a success of any mechanical undertaking is to have a thorough knowledge of the
basic principles involved.

The basic principles involved in the adjusting of an Underwood typewriter are surprisingly few.
It is the least complicated typewriter built.

A careful and thorough study of this book should make anyone, at all interested in the work,
a proficient typewriter mechanic.

&nbsp;

**HGP.**

# HOW TO REPAIR, REBUILD AND ADJUST UNDERWOOD TYPEWRITERS


## 01. Cleaning


The first step taken in overhauling, cleaning or rebuilding a typewriter, is to take the machine
completely apart, down to the base or main frame.

It is not necessary to take out the key levers, key lever locking rod [(**W\ Plate\ 2**)](#p2) back
space key lever or other parts that do not interfere with the cleaning of base.

Keep the screws and small parts in a box so that they will not get lost or mislaid.
Remember the screws as they are taken out so that they can be replaced in their respective places.
Brush the loose dirt out of base and from parts.

Use a pan large enough to take base and have brushes for cleaning and soak and clean thoroughly all
parts in gasoline with about 5% oil mixed with it.

The type bars and segment can be cleaned in alcohol, this will insure a better job.

In cleanings the segment make sure that there is no dirt or grit left in the slots.
After cleaning blow through the slots, this will take out the alcohol and any dirt there is with it.

On parts where the dirt is crusted on and cannot be taken off with a brush, use a scraper and
repaint the part with dull black paint.

After all the parts have been cleaned wipe off the gasoline as much as possible.
They will collect dirt and dust if this is not done.

If the key levers [(**A\ Plate\ 2**)](#p2) or key lever links [(**C\ Plate\ 2**)](#p2) look old and
the machine is to be rebuilt, it is a good plan to repaint them with the dull black, also any parts
that show.

It is seldom necessary to remove the key levers except in a case where the casting is broken and
should be replaced.
These come out by loosening retaining plate under machine held in place by two screws
[(**O\ Plate\ 2**)](#p2).

All the tension screws [(**N\ Plate\ 2**)](#p2) should be loosened to take tension off springs before
removing the key levers.

After all parts are cleaned and dried start to assemble and readjust the parts.


## 02. Key Levers


The key levers [(**A\ Plate\ 2**)](#p2) are all numbered from 1 to 42 on standard machines and from
3 to 40 on No. 4 machines.


These are placed in position, key lever tension springs [(**L\ Plate\ 2**)](#p2) are placed in holes
in base, over key lever tension screw [(**N\ Plate\ 2**)](#p2).

When a key lever is in position, see that it is perfectly free in the slot of comb at front of
machine [(**S\ Plate\ 2**)](#p2).

The key lever should not rub on either side of comb slot and should go up and down with the
slightest tension placed on spring.

See that the lever is parallel with comb slot and not twisted.
Key levers that rub or stick in any way will cause a sluggish action to type bar when operating
machine.
In freeing the key levers use a bender which is a round piece of steel with slot in the end to take
key lever.

This applied at the end of the key lever near point [(**X~1~\ Plate\ 2**)](#p2),
and the key lever is twisted one way or the other until
free and parallel with slot in comb.

If the lever cannot be freed with the bender, three prongs or nine prongs can be used to throw the
key lever to one side or the other until free.

Sometimes the slots in the comb will be closed up so that the opening is too narrow, this will cause
a bind also and they should be pried apart with a screw driver, but not so much that the
lever in the next slot will bind.

After all the key levers are in place the tension should be tried on each key lever individually.
This is adjusted by adjusting the tension screw [(**N\ Plate\ 2**)](#p2) so that the key lever, when
depressed will come back with a small click to the top of comb slot and stay there.

It should not come back with a dead spongy action.
If it is found that by using three prongs or nine prongs that the key lever is still spongy in action,
it will probably be found that the end of key lever is binding in bracket and will have to be
filed down a little.

In taking the key lever out loosen retaining plate screws [(**H\ Plate\ 8**)](#p8).
This plate holds the key levers in position so that they cannot jump out during operation of the
machine.

Take all the tension off the key lever tension spring and grasp the key lever with pliers at the end
and pull towards you and up at the same time. The lever should come out readily.

When replacing the key lever set the end in position over slot and use pliers pulling towards you
and forcing down. It should snap back into position in the bracket. The tension spring should be in
place before placing key lever.
After key lever is in place set spring over lug with spring hook.

Now tighten tension screw until the, key lever snaps up to the top of comb and stays there.
Loosen the key lever tension screws retaining plate [(**R\ Plate\ 2**)](#p2) whenever adjusting
tension screws.

The screws are set so that the flat side comes against this plate and the plate is to keep them from
working out.
This plate is fastened in with three screws [(**M\ Plate\ 2**)](#p2) which have springs over them.

As the key levers are important factors in the action of a machine, particular attention should be
paid in seeing that they are perfectly free, and that the tension is as light as possible without
having the key levers drop away from the top of slot in comb.

The black space key lever [(**H\ Plate\ 10**)](#p10) and the tabular key lever
[(**I\ Plate\ 10**)](#p10) have pins that come down on rests located on key lever comb.

See that the levers come down solid and do not go by. If they should go by the lugs are bent in or
out.

The back space and tabular key levers should be tried to see that they do not bind.
In trying the back space lever take the screw at [(**L\ Plate\ 8**)](#p8) out and raise back space
key top.
It should drop back without the slightest bind.

The tension on the tabular key should be about the same as that on the key levers.
The back space lever can be freed with the bender or three prongs.

Whenever it is found that the key tops are loose on key levers, they can be made solid by soldering
or wedging in place with prick punch.

The key ring glass and paper should be taken out before doing this.
Old key rings and dirty key papers should be replaced by new, if the machine is being rebuilt.

Anyways, have the key board looking neat and new.
The key tops should all be on a straight line with one another [(**J\ Plate\ 10**)](#p10).
Do not have one up and another down.

This is adjusted with peening pliers, applied under machine.
Peen the lever on the top to throw it down, and on the bottom to throw it up.

The typewriter should stand on its back as in [**Plate 8**](#p8), when freeing key levers,
or making other adjustments under machine.


## 03. Connecting Links


The connecting links [(**C\ Plate\ 2**)](#p2) can be assorted into groups of ten each.
That is the single Nos., 10's, 20's, 30's, and 40's separate.

This will make it easier and faster to replace them.
They are placed in the machine in rotation starting at the one marked No. 1, or No. 3, of No. 4
machines, placing slot over stud on key lever and dropping into place.

It is a good plan to emery cloth the part that goes into slot and oil same before placing
in machine. This will insure a free action.

The fulcrum wire [(**F\ Plate\ 2**)](#p2) is started in at the left side of machine and the links
placed, working the wire through as the links are placed.

After the links are in place they should be tested to see that they are all free.
These links also being part of the type bar action should not bind at any point.

Turn the machine up on the back as in [**Plate 8**](#p8), and proceed to free the studs on key levers that
go in link slots, should they not be central with connecting link.

There should be play on each side when tried with the finger, by pushing key lever to right or left.
This is adjusted with three prongs, applying at point [(**X\ Plate\ 2**)](#p2) throwing the key
lever to one side or the other.

After the lever studs are all central in the link slots try the key levers again to see that they
are still free.

Don't ever bend the connecting links to free them ; always free by adjusting side play at stud by
throwing key lever one way or the other.

If centering the link stud does not free the links up, it is possible that they are binding in the
bracket.
The round part of link can be filed a little to stop this.

Sometimes the link slot will not fit over stud on key lever without binding, in this case the slot
will have to be filed until free.

If the slot sides should get bent they can be straightened by laying on a flat piece of steel and
hitting with hammer.


## 04. Bell Ribbon Bracket and Lift Rail Spring Hanger



Attach the bell [(**J\ Plate\ 7**)](#p7), lift rail spring hanger [(**K\ Plate\ 10**)](#p10) and
bichrome shift lever bracket, back of [(**L\ Plate\ 10**)](#p10).

This, however, is very seldom taken off machine. 

See that there is enough tension on flat spring so that the red and black shifting keys
will stay in place when thrown.

Tighten all screws.


## 05. Ribbon Pawls and Shaft Assembled


Place the ribbon pawls and shaft assembled [(**K\ Plate\ 7**)](#p7) in slots of casting in main frame.
Place a drop of oil on each place. Fasten into place by two retaining plates.
Have the hump down when screwing in place.

There should be a little side play to this shaft to avoid having a sluggish ribbon action.
This play is adjusted by setting collar on end of shaft in or out.

Try the ribbon pawls [(**E\ Plate\ 10**)](#p10) to see that the tension spring is not broken and has
right amount of tension, which should be enough to keep the pawls together.

If the spring should be broken it is very easy to make a new loop to fasten by, thereby saving the
trouble of placing a new spring.

Tighten all set screws, as loose set screws will stop the ribbon from feeding correctly.

It is often found that the ribbon pawl oscillating arm [(**F\ Plate\ 5**)](#p5) is loose on shaft when
the ribbon does not feed correctly.


## 06. Escapement and Universal Bar


The escapement base is fastened in place with four 1/2 inch screws and should be set as tight as
possible.

The escapement should be adjusted and assembled before fastening to the base.
The base [(**DD\ Plate\ 5**)](#p5) can be repainted if needed.

The universal should be fastened after fitting the ears to the rocker arm [(**Y\ Plate\ 5**)](#p5).

The rocker arm is set in place between the two fulcrum points [(**BB\ Plate\ 5**)](#p5) so that there
is a minimum amount of play.

There should be no side play in the ears of universal frame at [(**EE\ Plate\ 5**)](#p5).
This is adjusted by bending the ears in or out with a bender as the case may be.

If there is no play at all there is liable to be a bind in the universal frame which will cause a
sluggish action.

If the ears of universal frame have to be bent, always try the rod that runs through to make sure
that it is free to turn around when tried.

This pin shaft [(**FF\ Plate\ 5**)](#p5) also should be free where it goes through holes in rocker arm.

The nut should be fastened on end tightly, being careful not to turn so hard that it will twist off
the end of pin.

To try the universal to see if it is free or not, raise it up with the spring off.
It should drop back freely and any bind will be felt when trying it with the hand.

After it is found that the universal is perfectly free, hook the spring to the pin in escapement
frame [(**U\ Plate\ 5**)](#p5).

The tension on the universal spring should be as light as possible and still have the spring in
action, which can be seen by watching where it hooks to pin in the escapement frame.
If the tension is too light it will cause a dead action to the type bars when operating.

Sometimes it will be found .that the tension is very light when the universal is against segment but
when the rocker arm is pressed the tension is very strong.

This can be eliminated by stretching the spring a little, giving it a longer action.
The tension spring on universal is adjusted by loosening set screw holding pin in escapement frame
and by pushing the pin in or out.

See that the assembled dogs [(**D\ Plate\ 5**)](#p5) are free, the loose dog is set with no play by
fastening pin [(**L\ Plate\ 5**)](#p5) as close as possible.

See that tabular block falls when lifted and that back spacer parts are not worn.

Fasten escapement to base and tighten screws.

In placing the assembled escapement, fit the slot at the end of rocker arm [(**Y\ Plate\ 5**)](#p5)
over stud on ribbon pawl oscillating arm [(**F\ Plate\ 5**)](#p5).

Try the ribbon pawl shaft after escapement is in place to see that there is still side play to the
shaft.

If there should be none, loosen set screw in ribbon pawls oscillating arm and set until there is side
play.

Take all the side play out of the assembled dogs, but not so much that they will not be free.
The dogs are tried for binding by pushing in on screw [(**HH\ Plate\ 5**)](#p5).
If the dogs are free they will snap back into position.

Tighten fulcrum set screws [(**AA\ Plate\ 5**)](#p5).

Sometimes dampness rusts the loose dog to the pin, so that the spring [(**W\ Plate\ 5**)](#p5) fails
to pull the dog back.

This stops the action of carriage and is adjusted by loosening set screw holding pin and taking pin
out and polishing off with emery cloth.

When replacing oil well; try the star wheel to see that there is enough tension on loose dog spring
[(**W\ Plate\ 5**)](#p5).

This is adjusted by loosening set screw holding pin and pushing out giving a longer action on spring.
If by pushing the pin out does not give the required tension, bend the pin back with pliers.

If the tension should be too light the loose dog would not drop back into position quick enough for
the carriage to run along without stopping and piling the letters one on top of another.

It is very seldom necessary to touch the screw [(**H\ Plate\ 5**)](#p5) holding the rigid dog in
place in relation to a tooth on star wheel. This will, in most cases, be found to set about right.

However, in cases where this screw does need adjusting, loosen set screw holding same which is
located on under side of escapement frame back of [(**E\ Plate\ 5**)](#p5) before attempting to
adjust screw.

If the screw should be turned without loosening set screw the threads will be stripped, and a new
screw must be fitted.

When setting the rigid dog it should be set so that the side nearest the star wheel will be about
3/64 inch away from the nearest tooth as it goes by.

Place a drop of oil on all the bearings. The escapement is the clock works of the typewriter and
should be adjusted correctly and a good deal of attention paid to it.

See that the star wheel is free, also that the nut [(**B\ Plate\ 5**)](#p5) is as tight as possible
without having star wheel bind.

On the older machines see that the wheel check spring is not broken and that the tooth on wheel
check is not worn.

The wheel check should he set so that it will just clear a tooth on star wheel when the rocker arm
is pushed out.

The tooth should set about central with a tooth on star wheel so that it will hold star wheel from
running back when banking carriage over to start a new line in operation.

If this tooth is worn and does not hold, the pointer on carriage will drop back one space when the
carriage is thrown over to starting point.

In cases where the star wheel bearing has become dry, take out the ball bearings, roll around in oil
and set back in vaseline, making sure that the play
is reduced to a minimum in the star wheel shaft, by adjusting nut [(**B\ Plate\ 5**)](#p5).


## 07. Back Space Connecting Link


Attach the back space connecting link [(**V\ Plate\ 5**)](#p5) to back space key lever arm
[(**L\ Plate\ 8**)](#p8) with double threaded screw.

Pull the spring attached over the first set of screw threads.

Before attaching spring, make sure that back space key lever is free to fall when lifted by key top.

Also see that none of the key levers at the left of machine rub or bind anywhere on the backspace
key lever.

Now fasten the back space connecting link [(**V\ Plate\ 5**)](#p5) to back space pawl carrier
[(**T\ Plate\ 5**)](#p5).

The spring can then he hook over the two pins, one on pawl carrier and the other
on the connecting link.

There should be a little play between the back space key lever and key lever comb.
This is tried by lifting key top up. If this play is not there the back space pawl will not lay back
on eccentric pin [(**S\ Plate\ 5**)](#p5) and is liable to pull forward so far that it will catch in
the spacing rack, stopping the carriage from moving.

This play is put in and taken out by adjusting the little arm [(**L\ Plate\ 8**)](#p8) by bending up
or down.

There is a special bender with slot to fit over arm for bending.

Also see that this arm lays central between two key levers so that the connecting link
[(**V\ Plate\ 5**)](#p5) does hot rub on any key levers.

If it should rub it would cause a sluggish action to the key lever it rubs on, also binding the back
space key.


## 08. Space Bar


Place the spring [(**C\ Plate\ 8**)](#p8) on space bar shaft, giving it one full turn after placing
the space bar wood under bumpers and fitting shaft in place.

The shaft is fastened to main frame by two pin screws, [(**D\ Plate\ 10**)](#p10).

These should be oiled where they go into holes of space bar shaft.

See that there is new live rubber on the striking pieces [(**F\ Plate\ 8**)](#p8),
[(**E\ Plate\ 8**)](#p8).

Set the throwing arm [(**D\ Plate\ 8**)](#p8) so that there will be the smallest amount of play
between it and the rocker arm on escapement, on which it acts.

If there is no play in this piece there is a tendency to hold the universal
away from the segment.
This alone would throw the machine into a spongy action.
If there should be too much play between these points the carriage would be liable to skip spaces.

See that the key levers do not strike the space bar levers when operating.

If the key levers should strike the space bar levers, the space bar levers can be bent to one side
with three prongs.
The best place to apply three prongs is at crook over rod [(**O\ Plate\ 8**)](#p8).

The top of the space bar should be about 1/4 inch below the lower row of key tops.
This is adjusted by bending the striking piece [(**E\ Plate\ 8**)](#p8) up or down.

Whenever this striking piece is adjusted the play in lever [(**D\ Plate\ 8**)](#p8) should be
readjusted.

The space bar wood should be changed or repainted if worn badly.


## 09. Segment


The segment, if in very bad condition, should be sent to the nickel platers for renickeling.
If not too bad it can be buffed up on a buffing wheel or if none is available, clean with metal
polish.
This should be done before it is cleaned in alcohol.

When attaching to cross bar use the long screws with large heads for the top holes and the next
length screws with smaller heads for the lower holes.

See that the tongue on universal that comes through slot in segment, does not rub or bind at any
place in slot.

If the tongue should bind it is adjusted by setting rocker arm to the right or left by setting
fulcrum pins [(**BB\ Plate\ 5**)](#p5).

Adjust these until the tongue is central in slot.

If the universal tongue should rub on the sides of slot it would cause a sluggish action to type bars
when machine is in operation.

Sometimes the universal will bind on the type bar hooks.
In this case the universal is raised by bending up with a bender.

This cannot be tried, however, until type bars are in machine.
The wing scales can be attached to segment at this time and adjusted later.


## 10. Shot Pad and Rest


Take the shot pad [(**G\ Plate\ 2**)](#p2) and lay on something flat and pound out all depressions
made by continual drop of type bars.
The shot pad and rest are fastened together by 4 pointed screws.

When fastening to main frame, pull the ends of rest out as far as possible before tightening screws
[(**L\ Plate\ 7**)](#p7).
These are the same as front name plate screws.


## 11. Type Bars


The type bars [(**V\ Plate\ 2**)](#p2) like the segment, if dirty and rusty, should be sent to the
nickel platers to be renickeled.

Clean in alcohol if they are good enough to place back in machine.

An ink eraser will make them fairly bright.

As the type bars are placed in machine they should all be filed a little at point
[(**X~3~\ Plate\ 2**)](#p2) to take off any burrs.

In putting them in machine the lever [(**E\ Plate\ 5**)](#p5) should be pulled over to allow them to
hook in over the rod.

When filing the burrs caused by type bars colliding, do not file so much that the bars go into guide
with too much play, merely file the burr off.
Play in the guide is detrimental to good alignment.

Take 5 bars of equal distance apart, say the (q), (d), (y), (I), and (?) bars
and place in the segment.

Loosen screws [(**L\ Plate\ 7**)](#p7) holding rest to main frame and set pad so that there will be
an equal amount of play in links, tried by holding type bar on pad and trying key top for play in link.

This play should be as small as possible and should be equal in order to have the type bars rest
evenly on the shot pad.

If there is too much play the bars will settle down in shot pad and lay unevenly.

After this is done, place the remaining bars in machine, adjusting play in connecting link stud as
they are placed.

See that the bars lay on shot pad in an even semi-circle. If the bars stand off the pad they are
adjusted by bending link above crook, lengthening it and adjusting link stud central with segment
slot by bending link over from bottom below crook at [(**X~2~\ Plate\ 2**)](#p2) until stud is central
with slot.

If this stud is not central with slot it will cause the type bar to bind. When placing the type bars
see that they are free in the segment slots.

This is tried by depressing space bar and throwing type up into guide.
It should drop back quickly and freely. The studs on connecting link should all be set central with
slots in the segment in order to keep the type bars free.

Place a drop of oil on each stud [(**E\ Plate\ 2**)](#p2).
Never twist the link to free except when absolutely necessary.

The connecting link end should be parallel with the type bar when it is in guide.

If it is not the link can be twisted to make it so. 

If the type bars cannot be freed by adjusting link studs, take the bars out and grind 
[(**D\ Plate\ 2**)](#p2) down with emery cloth until free.

Sometimes the bars will stick up in the guide [(**K\ Plate\ 2**)](#p2).
In this case bend the bar near type at [(**X~3~\ Plate\ 2**)](#p2) until tongue goes into guide and
drops back without sticking.

If the type bar strikes on the sides of the guide it can be bent central with the finger, forcing it
to the right or left.

In very rare cases a type bar will be found to be bent at [(**D\ Plate\ 2**)](#p2).
This can be remedied by laying on a piece of flat steel and straightening with a hammer blow.
If a type bar has once worked freely in a machine it will remain so unless it gets bent or the segment
slots get filled with dirt and grease.

It is bad practice to grind down or file old type bars at [(**D\ Plate\ 2**)](#p2) as they will get
thin and have a lot of play in segment slots, which is detrimental to good alignment.

Never squirt oil in the slots of segment. Oil the type bars by having a piece of felt saturated with
oil and rub the flat part of type bar on this.
They will pick up enough oil to keep them from rusting. If oil gets all over the segment slots in time
the segment will be a mass of dirt and grease.

To stop type bars from colliding, grind the sides of the type making them as thin as possible, being
careful not to grind any part of the characters.


## 12. Type Bar Guide


The type bar guide [(**K\ Plate\ 2**)](#p2) is an important factor in alignment.
If the sides of the guide [(**K\ Plate\ 2**)](#p2) are too far apart there will be a lot of play in
the type bars as they enter the guide.

A summary of all the bars should be made and the type bar guide closed up by means of set screw
[(**T\ Plate\ 2**)](#p2) to suit the majority of the type bars.
After this is done there will be several that still stick in the guide.

Sometimes these can be freed with the bender, but in cases where they cannot be freed with bender
the bar can be filed until it is free, and enters the guide without sticking.

Do as little filing as possible. The bars should enter the guide with the tongue
[(**X~3~\ Plate\ 2**)](#p2) parallel with the opening in guide and should not strike on the sides going in.

When trying the bars for freeness in the guide always use a light stroke.
A heavy stroke will cause depressions on the ring [(**J\ Plate\ 2**)](#p2) of segment.

A good way for trying the type bars in guide is to place a bit of white paper behind guide and the
tongue can be seen easier as it goes in.

If the tongue is not parallel with the opening it can be seen readily.
When segments are sent to the nickel platers, the ring [(**J\ Plate\ 2**)](#p2) should be draw filed
to take out all depressions.

Where there are depressions, those type will hit the cylinder harder when the machine is in
operation and cause the ink to show unevenly.

Try the (p) and (q) all the way across alternately when the carriage is on and see that they are in
absolute alignment. If one is lower or higher than the other it is adjusted by throwing the type bar
to one side until they are even by adjusting screws in holes [(**Y\ Plate\ 2**)](#p2).
They are just above those screws that fasten guide to segment.


## 13. Trip and Alignment


The trip is tried by holding the star wheel with the finger, placing tension on it, the same as if
the carriage were running over the pinion, and then raising the type bar to the guide.

The star wheel should trip around when the bar is about 3/4 of the way in the guide or about
3.16 inch away from the face of the cylinder when in position.

The trip should be tried with the end bars and a middle bar and should trip in the same position for
the three bars.

If the bars should trip closer on one side than on the other it is because the type bar on that side
is throwing the universal sooner than on the other side and the adjustment is made by throwing that
end of the universal back by bending at point [(**M\ Plate\ 7**)](#p7) and bending forward if the
bars are tripping nearer than on the opposite side.

If the bars should trip closer in the center than at the
end, a screw driver can be forced down beside tongue and forcing the universal out at that point.


After the trip has been evened off all across, a trip wrench is used for the final setting of trip,
by tightening or loosening the trip nut on dogs [(**GG\ Plate\ 5**)](#p5).

If this trip nut is found to be too loose and turns easily, the dogs are taken out and the trip
screw taken out and the hole closed to make smaller by hitting with hammer.

A machine in operation with a loose trip nut won't run very long before the trip gets out and then
the carriage will start to pile up the letters. The alignment is tried by taking two end bars and a
center bar.

Hold the universal against the segment from the back and depress the three bars at once.
The center of the face of type should come about 8 inch away from the end of type bar guide when
the type bars are up and stopped by universal which you are holding against segment.

This is adjusted by sliding piece on universal frame [(**N\ Plate\ 7**)](#p7).

This distance between type face and guide is adjusted by setting the sliding piece in or out by
loosening set screws and moving.

If it is finally found that the type bars trip unevenly all the way across, set the trip to the
majority of bars, seeing that all trip before they get to the platen or cylinder.

When the trip is too close to the cylinder the letters sometimes pile.


## 14. Lift Rail


Take the lift rail [(**O\ Plate\ 7**)](#p7) and fasten in vice. Use an emery stick or some flat
surface with emery paper and grind down the surface of rod where the carriage wheel runs along.

This is done to take out any unevenness caused by long use of machine.
The rod can be repainted with dull black to bring back the appearance.

The lift rail is put in machine by working it in over the escapement, holding the left end down as
you face the rear of machine.

Set fulcrum pins in place and set rail so that groove for star wheel nut is central.
There should he as little space as possible between the arm running down to shift lock at stud and
casting.

See that the shift key retaining hooks on lift rail ears take the shift key lever ends that throw
lift rail up.

Do not set lift rail so that the shift key ends come on one side of the hooks.
Tighten fulcrum set screws so that there is no side play to the lift rail, but still free enough to 
have the lift rail fall freely when lifted.

Fasten lift rail spring to hanger [(**K\ Plate\ 10**)](#p10) and set about 4 notches from the bottom
in teeth on lift rail.

A drop of oil on fulcrum points will keep them from rusting and binding.


## 15. Main Spring


The main spring [(**P\ Plate\ 5**)](#p5) is set in place, working in around lift rail so that
spring drum [(**N\ Plate\ 5**)](#p5) is on one side and the ratchet [(**A\ Plate\ 5**)](#p5) on the
other.

See that the ratchet wheel release handle [(**A\ Plate\ 5**)](#p5) screw is tight before fastening in
place.

To tighten this take out screw holding handle [(**O\ Plate\ 5**)](#p5), take handle off and the main
spring drum will separate from the casting and you can get at the screw.

In fastening the spring barrel in place you use the shortest length screws like those in lower part
of segment.

Try the spring drum to see that it don't rub on lift rail when rail is lifted or lowered.
See that lift rail is over to the right far enough, looking from the back so that the drum does not
rub on same.

It should revolve freely without rubbing. In case the drum should rub anywhere, it is adjusted by
filing the casting on one side of the screw holes to make it throw over when the screws are tightened.

When taking the main spring apart be careful not to lose the ball bearings. If the main spring is
broken, the drum plate can be pried off and the old spring repaired or a new one put in.


## 16. Front Rods, Bell Trip and Locks


Set the back rod [(**P\ Plate\ 7**)](#p7) first. On the left there is a bushing that goes in casting
which the front rod goes through.

Push the rod through bushing. Place the right marginal stop.
See that there is a little play in the teeth on the rod. Push rod through the hole on opposite side
and fasten with round nut.

In fastening see that the teeth on rod are parallel with those on marginal stop.

Before placing the smaller front rod, try the bell trip rack [(**R\ Plate\ 7**)](#p7) to see that it
swings freely on rod.
This should not bind in the least. After trying this, place the rod through bushing at left.

Place round spring, run through first hole in bell trip rack, put on bell trip guard
[(**P\ Plate\ 10**)](#p10) left marginal stop and collar, on older machines the next part is the other
hole in bell trip rack, another collar and then through right marginal stop and casting, and it is
then fastened with round slotted nut.

On the newer machines after placing collar and left marginal stop, the rod is run through right
marginal stop, collar, bell trip rack end, then collar between rack end and casting.
It is fastened with round nut same as back rod nut.

There should be play in the teeth of left marginal stop the same as for right.
If the stops should bind and not work on rod freely, oiling will free them or sometimes turning the
front rod around a little.

The round spring at left should be hooked under pin on bell trip rack and the end pushed under
casting.
Set the left marginal stop so that teeth snap into second tooth on bell trip rack.

Set the collar to hold stop in place. Set the right.marginal stop so that the teeth snap into the
last tooth on back rod and fasten collar into place to hold same in position.
The teeth should click into place when throwing the stop handle and releasing.
Place the pointer on marginal stop, seeing that the screws do not bind the stop to the rod.

In adjusting the bell trip arm that runs down inside of casting, see that the tumbler at end comes
in center of bell hammer arm. Also see that the push release button acts on this arm forcing it out
a little when left marginal stop handle is raised. This is taken up under line lock.

See that there is enough tension on round spring to bring bell trip rack [(**R\ Plate\ 7**)](#p7)
back when pushed down.


## 17. Way Rod


The way rod is placed on machine after it has been put in a lathe and smoothed down with emery cloth
to take off rust and polished.

A good plan is to grease the way rod with a combination of oil and vaseline.

In many of the longer carriage machine there is sometimes found trouble with the carriage binding
on way rod.
On these machines it is better to grind the way rod with the lugs off to insure an even grinding,
all along.

To take the lug off drive the pin out. When replacing file off any burr that is left, so that the
carriage bushings will slide over freely.


## 18. Carriage Frame


Take the carriage spacing rack and center the holes by running cylinder rod through, so that rack
swings freely on cylinder rod.
The rack is then placed over screw on left of carriage and fitted at the right.
The rack should fit snug and have no side play whatever at the screw shoulder [(**N\ Plate\ 10**)](#p10).

This is tried by forcing rack out when screw is in place. If there is play found there the screw can
be milled down in hollow mill, No. 124. If you mill the screw too much fit a new screw.

Filing or counterboring the rack is bad practice although it is done in some cases.
If this rack should have end play it would help to cause bad side alignment after the machine is in
operation.

Place thumb plate and filler on carriage frame.
This thumb plate is replaced by a new style rack release lever on newer machines.

Place the right margin release [(**T\ Plate\ 10**)](#p10) seeing that there is no bind and that it
snaps into place when depressed.

See that spring is not broken or weak.
Place carriage pointer, and carriage guide [(**B\ Plate\ 10**)](#p10), also carriage locking piece
[(**B\ Plate\ 3**)](#p3) which is fastened in place by two concave head screws [(**Y\ Plate\ 3**)](#p3).
Should screws with round heads be used they would bind on right margin stop as carriage is pulled over.

Fasten spring [(**O\ Plate\ 10**)](#p10) to stud, seeing that there is plenty of tension on same.
Envelope holder is fastened in place to bracket with pin, the ends being mauled after it is in place.

Place the carriage on way rod.
Place draw band on spring drum and fasten to lug on carriage bushing [(**U\ Plate\ 3**)](#p3).

Give main spring handle about three full turns.

Try space bar and see that it comes down on bumpers [(**F\ Plate\ 8**)](#p8) about 1/8 inch after the
carriage trips.
If the space bar does not come down far enough, bend the bumpers down until it has the 1/8 inch drop
after the trip.

If this drop is not there the carriage is liable to skip spaces during operation of machine.
See that the spacing rack does not bind anywhere.

In trying the carriage to see if there is enough tension on main spring, see that the pointer on
carriage goes to last space without any drag.

See that the front carriage roll is perfectly free and place oil on same.
If the carriage hesitates near the end of the writing line give the main spring handle another half
turn.

Set rack on pinion as low as possible without having it grind over pinion.
This is adjusted by adjusting screw in tabular block [(**Z\ Plate\ 5**)](#p5) located on top of block
and is held by set screw in side of block.


## 19. Line Lock


The line lock rod [(**W\ Plate\ 2**)](#p2) runs under the hooks on the key levers locking them towards
the end of the writing line, four spaces from the end.

This is controlled by the left marginal stop, which is thrown by locking piece [(**Z\ Plate\ 3**)](#p3)
on carriage and operates at any position where the stop is set.

The locking rod is connected with the locking shaft [(**O\ Plate\ 8**)](#p8) by two arms that run down
through key levers and the locking rod rests right over the key levers just so that the key lever
hooks clears it.

An arm coming up through the key levers in the center of the locking rod with two prongs
[(**P\ Plate\ 8**)](#p8), for the rod to rest between.

The locking rod is borne towards the front by means of a spring attached to this arm that runs up
through the key levers.

The rod [(**W\ Plate\ 2**)](#p2) when tried with the finger pressing it forward should spring back
freely without any bind.

If there should be any bind the key levers would lock every time the locking piece on carriage hit
the locking point on left marginal stop and would stay locked when the carriage was returned.

The locking shaft [(**O\ Plate\ 8**)](#p8) is made up of a locking lever [(**G\ Plate\ 8**)](#p8)
with a spring which goes in back of casting.
This spring holds the locking rod away from the key lever hooks. This must always have enough
tension to throw the locking rod back out of reach of the key lever hooks, when the locking rod is
absolutely free and not binding or rubbing any where.

Next comes the collars with shoulder which the locking rod connecting arms hook to.
In the center of locking shaft is the arm that runs up through key levers [(**P\ Plate\ 8**)](#p8)
next to that the other collar with shoulder to take locking rod connecting arm, next a collar with
a pin or long headed screw which adjusts the locking rod in relation to position near key lever hooks
this collar, also adjusts play in locking shaft which should be small.

The collar [(**Q\ Plate\ 8**)](#p8) is set by screw adjustment, and should be tight.

The locking rod should never be more than 1/16 inch away from the key lever hooks.
The collar [(**Q\ Plate\ 8**)](#p8) should be set over so that there is as little play to the locking
shaft as possible.

See that none of the connecting arms running through the key levers bind.
The locking arm at the extreme left [(**G\ Plate\ 8**)](#p8) should be set so that it runs parallel
with the connecting arm on locking rod [(**W\ Plate\ 2**)](#p2).

The bell trip arm running down into casting from bell trip rack throws this when the pointer on the
carriage is opposite pointer on marginal stop, and this throws the locking rod under the key lever
hooks when adjusted properly, locking them, for the first lock.

The keys cannot be used until the push release [(**M\ Plate\ 10**)](#p10) is pressed in, which after
releasing allows the carriage to travel four spaces further when the second
lock should take place.

When the push release is pushed in it should push the bell trip arm out, releasing it from locking
lever [(**G\ Plate\ 8**)](#p8) which lets the locking rod spring back from the key lever hooks.

This locking lever should be adjusted so that when trying the locking rod [(**W\ Plate\ 2**)](#p2)
there should be a little play between it and the key lever hooks, when the keys are locked.

This should be a minimum of play as the second lock cannot be correctly if there is too much play.
This play is adjusted by loosening set screw in [(**G\ Plate\ 8**)](#p8) and throwing lever forward
or back.

It is thrown forward to give play and back to take play out.

Make sure that the bell trip tumbler is still central with the bell hammer arm.
Also see that the pin on tumbler does not hit locking lever [(**G\ Plate\ 8**)](#p8) so full that
the push release will not release it.

This is adjusted by forcing the locking lever sideways.
The bell trip arm is adjusted by bending with a special bender with a crook bent go degrees and a
slot in end so that it will go in back of casting to grip the bell trip arm.

The bell trip arm should be held against the push release when bending the end so that the upper
part can still be forced out when the push release is depressed.

If the push release rubs so hard that it sticks, the bell trip arm can be bent from above to throw
it away from the push release and adjusted at the end to set tumbler afterwards.

The bell trip arm after being released from the first lock should jump back on the third space
ready to take the second lock on the 4th space.

On the second lock there should be a little space between the locking piece on carriage and the
end of marginal stop bank.

On the release of the 2nd lock, when a type bar is struck the rigid dog should hit into a tooth
on the star wheel and prevent printing of the type.

This is adjusted by sliding piece [(**B\ Plate\ 3**)](#p3) on carriage to the right or left until this
takes place.

If the second lock cannot be made by adjusting the locking arm [(**G\ Plate\ 8**)](#p8) the contact
point [(**Z\ Plate\ 3**)](#p3) of locking piece can be bent down a little.

The two locking points should be on a line with each other and can be tested by the amount of play
in locking rod [(**W\ Plate\ 2**)](#p2) on each lock.

The play should be the same for both locks. If the locking piece on carriage is set too far over you
will not get the second lock.
In this case the locking piece should be moved to the right.
If you should happen to get the lock when it was too far to the left you would find that the two
pointers would not coincide.
The carriage pointer will be one space beyond.

The lock should take place 4 spaces from the end when the marginal stop is over as far as it will go.
The locks should also work when the marginal stop is over to about 40 on front scale
[(**B~1~\ Plate\ 10**)](#p10).

Sometimes the bell trip rack gets twisted out of shape and should be twisted back until the locks
work at both points.

Bend or twist with two wrenches carefully. The pointer on the carriage should be in a line with that
on the margin stop when the lock takes place.

The bell should ring 7 spaces before the 1st lock is made, and is adjusted by the position of
[(**Z\ Plate\ 3**)](#p3).
If this point is raised too much the bell will not ring until the 1st locking point is hit.

If it is bent too low it will ring before the 7 spaces.
Also the carriage will bind, or will lock before it hits the locking point.

In order to adjust the locking piece on carriage for bell, run carriage over so that the two pointers
coincide and bear down on carriage, then raise handle of marginal stop.

There should be a little play between locking points on marginal stop and the points on locking
piece [(**Z\ Plate\ 3**)](#p3) on carriage.

This can also be tried by raising handle of marginal stop, holding and running carriage over locking
points.

The carriage should go by without rubbing.
Whenever bending the locking piece on carriage always readjust the locks as they will probably be
thrown out by the bending of the piece [(**Z\ Plate\ 3**)](#p3) up or down.


## 20. Right Marginal Stop


The right marginal stop should be set at 10 on the front scale and the carriage banked over against
the carriage stop on marginal stop to see that the carriage does not fall back or go beyond one space.

Should the carriage go beyond one space, washers are set on the right of back rod between shoulder at
point [(**I\ Plate\ 7**)](#p7) and casting.

If it is found that the carriage falls back one space it is because the wheel check is not holding
the star wheel from running back when the carriage is pulled over, or it may be found that teeth on
the pinion are broken or that pinion pawl springs are broken off.

If the washers have been placed on the rear rod, the right marginal stop collar will
have to be readjusted so that the teeth on marginal stop will snap into teeth of back rod with a
click when the marginal stop is set to the right.

The wheel check [(**D\ Plate\ 5**)](#p5) is on all the latest style machines, and dropping back of
the carriage is caused by wire spring being broken or misplaced, or it may be found that the assembled
dogs are set too far over.

The end of spring in wheel check [(**D\ Plate\ 5**)](#p5) should be placed in slot at bottom of dog
frame.


## 21. Front Scale Plate


The scale plate if new or reenameled should be scraped on all bearing points and also in slot where
pin on locking piece [(**Z\ Plate\ 3**)](#p3) on carriage frame runs along.

Enamel bulging out in this runway may cause the carriage to stick when the machine is in operation.

In placing the scale plate on machine pull forward as far as it will go before tightening screws
[(**S\ Plate\ 7**)](#p7).

There should be the smallest amount of up play between the pin of locking piece
[(**Z\ Plate\ 3**)](#p3) of carriage and runway of scale plate in order to have the carriage run
freely.

If it is found that there is no play and that the carriage binds, the pin can be filed off until the
required play is there.
If there is very much play in the locking piece it will affect the alignment to a certain extent and
the play should be taken out by either peening the pin to make it longer or in rare cases the whole
piece [(**Z\ Plate\ 3**)](#p3) can be bent up.

However, if in bending the piece up it is found that the bell don't ring 7 spaces before the first
lock takes place, a new pin will have to be fitted.

The only time when the locking piece [(**Z\ Plate\ 3**)](#p3) should be bent up is when the pin has
been peened so much that it is impossible to lengthen it any more to take out the play.

If it is found that on trying for play that there is play at the ends of scale runway and none in
the middle, the scale plate must come off and be bent in the middle over the knee or something bulky.

Do not make a short bend, and be careful not to bend too much as it will have to be bent opposite,
if there is play in the middle and none at the ends.

It takes time to make these bending adjustments and should be bent a little at a time and tried in
machine each time to see that the bending is done correctly.

Never bend the plate on machine by forcing up with screw driver as this will lead to other and more
serious trouble.

Never file the casting at points where the scale plate rests to bring it forward.
You can always fit a new scale plate without much expense but if you should happen to file the
casting too much the cost would run pretty high.

There is no need for filing the casting, expect when a new base is assembled.
And then only to remove enamel where it has run onto bearing points.


## 22. Right Marginal Release


The right margin release [(**T\ Plate\ 10**)](#p10) when depressed should snap back absolutely free
when it is set in place tightly by screw [(**J\ Plate\ 3**)](#p3).

If it should bind at all it can sometimes be remedied by bending at different points and trying.
When the release is brought over the marginal stop, space the carriage and see that the end of
release strikes the incline on marginal stop so that it rides over without pushing the marginal
stop along.

If it pushes the marginal stop the end is set too low and is corrected by placing release on two points
such as two jaws of a vice and is hit in the center on the narrow edge.

Bend a little at a time and place on carriage and try until it rides over the incline without pushing
the marginal stop along.

In placing a new carriage stop [(**T\ Plate\ 7**)](#p7) the point where the carriage banks against
after releasing the right margin release, will have to be filed down until there will be a little
play between the carriage when pulled over to the right and the right angle piece
[(**T\ Plate\ 7**)](#p7) after the right margin release is depressed.

The carriage, in other words, should travel a little further after the margin release is depressed
when the the pointer is set at 0 and the carriage brought up against the marginal stop.

These stops are made longer as hardly any two machines are alike in this respect and the stops should
be filed to fit each machine.

If on bringing the carriage back to the start, with the pointer set at 10, the carriage should go by,
it is probably caused by a bind in the marginal release lever or it may be found that the flat spring
on same is broken off, or weak When fitting release to a new carriage frame all bearing points
should be filed to take enamel off.


## 23. Trip, Loose Dog and Rigid Dog


The trip should be tried again after the carriage is set on machine with draw band attached, to see
that it has not changed from the first setting.
If it is found to be out, adjust by tightening or loosening trip nut [(**GG\ Plate\ 5**)](#p5) in top
of dogs.

After checking up the trip, depress one of the key levers and look at the loose dog, for position in
relation to teeth of star wheel.

The face of loose dog should be set, adjusting screw [(**J\ Plate\ 5**)](#p5) in or out, so that it
rests a little to the left of center of the space between two star wheel teeth:
If the loose dog travels too far or not far enough the carriage will pile or not run at all.

Tightening screw [(**J\ Plate\ 5**)](#p5) throws the loose dog to the left and loosening brings it
to the right.

If it is found that by tightening or loosening does not affect the position of the loose dog, it will
be found that the assembled dogs are set too far to the left and should be moved over a little.

The screw [(**HH\ Plate\ 5**)](#p5) should be adjusted so that when pushed with a screw driver,
holding a key lever down, there should be a little back play.

If it is found that there is no back play, loosen screw until there is.
If there is not any back play and the screw should be in too far there would be a sluggish action to
the machine in operation, also the space bar when depressed can be felt to be forcing something.

If there is too much back play it will often be found that the carriage is skipping spaces.


## 24. Back Spacer


Try the play in back space key lever to see that play between lever and comb is still there.
This keeps the back spacer on escapement back against the eccentric pin [(**S\ Plate\ 5**)](#p5).

The back space wheel check [(**M\ Plate\ 5**)](#p5) should be perfectly free to swing around.
This has to be fitted when placing a new one.

The wide end is placed in slot on back spacer and the end that is bent down should come under a
tooth on star wheel when back spacer is used.
This bent part should come under the star wheel tooth so that star wheel does not run back when
back spacer is being used.
It also should not run into the tooth on star wheel.

If it hits into a tooth or there is too much play so that the star wheel runs back, the piece is
adjusted by bending up or down as the case may be.

The wheel check [(**M\ Plate\ 5**)](#p5) should also come central with a star wheel tooth.
If it does not go way under the tooth it wears very quickly. This is adjusted by turning eccentric pin
[(**S\ Plate\ 5**)](#p5) or by forcing under with a screw driver, bending piece.

There is a hook at top of back spacer that fits over eccentric pin and the back spacer pawl is
adjusted into the teeth of spacing rack by adjusting eccentric [(**S\ Plate\ 5**)](#p5).

The tooth of the back space pawl should protrude from the spacing rack teeth about 1/64th of an inch
when the back spacer is used.

If the pawl goes into the rack teeth too far there is a liability of the pawl sticking there and
stopping the carriage spacing after using back spacer.

Whenever adjusting the eccentric pin [(**S\ Plate\ 5**)](#p8) always look at back space wheel check to
see that it is central with the tooth on star wheel when the back space key lever is depressed.


Also see that it does not go under the star wheel too far as this will also stop the carriage from
spacing should it catch on the star wheel by not returning far enough.

If the back space pawl [(**T\ Plate\ 5**)](#p5) should raise the spacing rack during operation,
the top of the tooth can be ground off a little or possibly the spacing rack is set too low on pinion,
which can be found by holding spacing rack down with finger and running carriage over.

If there is a decided grind, the spacing rack should be raised until this grind is taken out by
adjusting set screw in tabular block [(**Z\ Plate\ 5**)](#p5) which eccentric pin
[(**S\ Plate\ 5**)](#p5) holds to escapement.

Sometimes the carriage will go back two spaces when using back spacer.
This can be stopped by placing washer between collar holding back spacer hub on dog fulcrum pin
[(**Q\ Plate\ 5**)](#p5) and the back spacer hub.

If it is found that there is already a washer placed there, the running back of two spaces may be
adjusted by loosening set screw holding eccentric [(**S\ Plate\ 5**)](#p5) and forcing the whole
back spacer out from the top [(**X\ Plate\ 5**)](#p5).

Do not force this out so far that the back spacer will bind on eccentric pin.

If this does not remedy the trouble try another washer on the dogs fulcrum pin.
If the back space pawl tooth goes too far into the spacing rack teeth and cannot be adjusted by
turning eccentric pin, bend the pawl carrier [(**T\ Plate\ 5**)](#p5) back until the tooth protrudes
a little.

If the pawl should operate with a decided click, it is caused by the pawl not pulling forward far
enough before starting the side motion. This can be adjusted by the eccentric pin, or sometimes by
forcing the pawl carrier out a little.

If the back spacer does not go into the teeth on spacing rack but strikes into them try forcing the
whole back spacer piece back towards the escapement by placing screw driver in screw slot of screw
that holds spring [(**C\ Plate\ 5**)](#p5) to back spacer and hitting with hammer.

In rare cases the part fastened to pawl carrier [(**T\ Plate\ 5**)](#p5), that is, the part pushed
back when pawl goes into rack, can he ground off a little at point where it stops pawl moving
sideways before it hits spacing rack, to release pawl carrier [(**T\ Plate\ 5**)](#p5) sooner.

If back spacer doesn't pull to one space, take up all play in key lever and comb by bending arm
[(**L\ Plate\ 8**)](#p8) down.

Raise comb as high as possible and take out all back play between wheel check and star wheel tooth.


## 25. Automatic Ribbon Reverse


The spool carriers [(**X\ Plate\ 4**)](#p4) should next be taken and the parts assembled to them.
When placing the upright shafts [(**O\ Plate\ 4**)](#p4) see that there is a little play to each when
setting the collars.

Place the shifting pawls [(**B\ Plate\ 4**)](#p4), friction spring [(**A\ Plate\ 4**)](#p4), and gear
[(**D\ Plate\ 4**)](#p4) before setting lower collar.

There should be enough tension on friction spring [(**A\ Plate\ 4**)](#p4) to allow the shifting pawls
[(**B\ Plate\ 4**)](#p4) to work around into shifting wheel [(**E\ Plate\ 4**)](#p4) to make the shift.

Do not have so much tension that it will cause the ribbon to wind sluggishly.
See that both shifting pawls [(**B\ Plate\ 4**)](#p4) take tongue of shifting arm
[(**J\ Plate\ 4**)](#p4) in slots firmly so that when pulled with finger shifting arm
[(**J\ Plate\ 4**)](#p4) will not pull out.

If they should pull out hold shifting arm against slot and force [(**H\ Plate\ 4**)](#p4) out and then
pull [(**H\ Plate\ 4**)](#p4) back as far as it will go to the front.

Tighten all set screws and try again to see if the shifting arms pull out.
If they still come out try bending the arm up or down a little in the slot to give the grip in a
different place.

If this does not work, file the piece a little to give it a hold.
These arms should not come out of position in the slots of shifting pawl [(**B\ Plate\ 4**)](#p4)
until the eyelet on ribbon throws the arm [(**H\ Plate\ 4**)](#p4) out.

If the shifting arms are not set correctly the ribbon will reverse and jam when the machine is in
operation before the end of the ribbon is reached.

In trying the spring [(**A\ Plate\ 4**)](#p4) to see if there is enough tension on same, push the
gear to the opposite spool carrier and release [(**H\ Plate\ 4**)](#p4).

Now turn the upright with the finger; the shifting pawl [(**B\ Plate\ 4**)](#p4) should travel around
when turning.

On the shifting arm [(**H\ Plate\ 4**)](#p4) shaft there is a spring that is adjusted by collar
[(**M\ Plate\ 4**)](#p4) which should, be set so that when the arm [(**H\ Plate\ 4**)](#p4) is thrown
out it will spring back into place.

The cup that holds ribbon is held in place by a set screw and should not bind but have just a little
play as it is turned around when placing ribbon in slot of [(**H\ Plate\ 4**)](#p4) and should spring
back into place when released.

After the parts are assembled to the carriers, the carriers are fastened to main frame with screws
the same as the lower segment screws but a little shorter.
If the longer screws should be used the carriers will not fasten in place perfectly solid.

After the carriers are in place take the winding shaft [(**P\ Plate\ 4**)](#p4) and start through
from left, placing left gear [(**D\ Plate\ 4**)](#p4), detent hub [(**C\ Plate\ 4**)](#p4) and
gear for right carrier [(**D\ Plate\ 4**)](#p4) as the shaft is placed in position.

See that the tension on shifting wheel [(**E\ Plate\ 4**)](#p4) at left is strong enough to hold wheel
firmly in position.

Place the right shifting wheel [(**E\ Plate\ 4**)](#p4) up against shoulder on shaft
[(**P\ Plate\ 4**)](#p4) and tighten set screw.

Place ratchet winding wheel [(**K\ Plate\ 4**)](#p4) so that the end of shaft is about flush with hub.

If the winding wheel [(**K\ Plate\ 4**)](#p4) is set in too far it will bind on shift key lever
throwing shift out, causing the letters to jump around when making caps.

Push the shaft to the left as far as possible and set the detent hub [(**C\ Plate\ 4**)](#p4) in place.

Set the bevel of [(**C\ Plate\ 4**)](#p4) on the left of detent lever point [(**Q\ Plate\ 4**)](#p4).
There should be a little play on each side of [(**Q\ Plate\ 4**)](#p4) when the shifting rod is
tried on both sides.

The amount of play should be equal and brought to a minimum.
It is first equaled up on both sides and then reduced by adjusting screw [(**G\ Plate\ 4**)](#p4).
See that there is plenty of tension on detent lever spring [(**F\ Plate\ 4**)](#p4).

If this is too weak the ribbon will shift every time the carriage is pulled over if it hits the stop
with a jerk when the ribbon is winding on the left spool.

Fasten the gears in place so that they mesh evenly and a little way from the gear on carrier shaft
so that there will be no grinding of gears when tried by winding.
The carrier gears should set about 1/64 inch higher than the gears on shaft.

Adjust the ribbon winding pawls [(**E\ Plate\ 10**)](#p10) so that they are three teeth apart on the
winding wheel [(**K\ Plate\ 4**)](#p4) and fasten into place with set screw.

Depress a key lever, watching the pawls at the same time.
The upper pawl should drop about 3/2 the distance between one of the teeth on winding wheel,
when the key lever is released so that the ribbon will feed correctly.

In trying the ratchet winding wheel to see that the pawls are feeding correctly, hold the winding
wheel back slightly and strike a key lever several times rapidly.

The pawls should act and send the winding wheel around a tooth at a time. If the ribbon pawls
[(**E\ Plate\ 10**)](#p10) should be set too many teeth apart, they would drop so far that they might
hit on casting and cause a sluggish action to machine.

It would also be difficult to take the type bars out of segment.

When the lever [(**E\ Plate\ 5**)](#p5) is released and the rocker arm [(**Y\ Plate\ 5**)](#p5) is
pushed in as far as it will go, it throws the ribbon pawls down further than when the machine is
in regular operation.

If the pawls are set too far apart they will strike the casting before the lever
[(**Y\ Plate\ 5**)](#p5) is way in and as the arm [(**Y\ Plate\ 5**)](#p5) has to be pushed in the
full distance in order to take the type bars out, it is essential that the ribbon pawls are set
correctly, and have a little drop beyond their normal drop.


## 26. Carriage Lift Hooks


The carriage lift hooks [(**F\ Plate\ 10**)](#p10) are fastened to the shaft with two pins.
The pins of one end have to be driven out before the shaft can be removed from carriage.

To take the lift hook shaft out drive out the pins on one end, take the end off, loosen set screws
in tension spring [(**G\ Plate\ 3**)](#p3) and the shaft will pull out.

When placing the eccentric bushings [(**H\ Plate\ 3**)](#p3) in machine, set so that the thin
portion is at the top.

If the thin part is set down the lift hook shaft is liable to bind on carriage frame casting.
This would also set the whole inner carriage out of position when it is placed.

There should be absolutely no side play in the lift hook shaft.
This is taken out by forcing the eccentric bushings [(**H\ Plate\ 3**)](#p3) out at each end.

These bushings are held tight by two set screws.
These same bushings govern the position of the cylinder in relation to face of type on bar.

To see that the lift hooks are free loosen the set screws in spring [(**G\ Plate\ 3**)](#p3), set
the eccentric bushings out to take out play and raise the lift hook up.

It should drop back to its normal position without binding.
If there should be a bind tap the end of shaft with a hammer until it is free, but not so much that
it will put play in the shaft.

If there is a bind when there is side play it will probably be found that the shaft is bent.
It will then have to be taken out again and straightened, trying in the bushings until free.

When in place, fasten collar [(**A~1~\ Plate\ 3**)](#p3) into position.
This is done with the lift hooks dropped to their normal position and the collar is forced
against the spring pushing the spring in a little and tightening set screw, having it point to the
front of machine and upward a little.
The reason for forcing the spring in a little is that in placing tension on the spring shortens it
and this is to take up that shortening.

Wind the spring up to about 6 teeth and fasten set screw in spring hub next to the set screw on
collar.

If after the carriage is assembled and in place it is found that the shift is too heavy and that the
lift rail tension spring is in the bottom notch on lift rail, the spring [(**G\ Plate\ 3**)](#p3)
can be turned another tooth or if the tension is too light it can be let down a tooth.


## 27. Skeleton or Inner Carriage


Before placing the inner carriage, take the cylinder shaft and run it through the bushings at each
end of carriage to see that the holes are lined up with each other.

The rod should go through from both ends freely so that the cylinder shaft will spin around when
twirled.
If the holes are not lined up the cylinder would be liable to bind in turning.

If the cylinder binds in turning, it will cause uneven line spacing if all the adjustments are not
absolutely correct.

To adjust these bushings when they are not lined properly place the cylinder shaft through one and
run it to the other bushing but not through and see that the end of shaft is central with the
opposite bushing ready to go in without rubbing on sides.
If it is not central bend the bushing the shaft is through, with the cylinder rod until it is central
with the opposite bushing.

Try the tension on the feed roll hangers [(**Q\ Plate\ 3**)](#p3) seeing that the tension is about
equal for all of them.

In adjusting the hangers, loosen the set screws in hubs and force the hub around, placing tension on
hanger, making all as even as possible.

The final adjustment is made with the hexagonal headed screws in ends of hangers after the cylinder
is in place.

Take the feed rolls assembled and place a little oil on each bearing.
Do not get the oil on the rubber parts of the feed rolls.
Clean the feed rolls thoroughly with alcohol after oiling.

If the feed rolls are not oiled and cleaned, there is a liability to be trouble with paper feeding
when machine is in operation.

On later machines the rear or large feed rolls are set in place with collars [(**B~1~\ Plate\ 3**)](#p3);
they should be set so that each feed roll has a small amount of side play.

Also set so that the feed roll comes central with the openings in paper table [(**E\ Plate\ 3**)](#p3).
This is done after the feed rolls are placed in hangers.
The large feed rolls are removed from shaft by taking out end screw and loosening set screws in
collars.

On the front set it is better to replace with new complete set as the feed rolls on these are set
with permanent collars that are sweat on.

On the very old machines some are found with composition feed rolls.
These are held in position with filler that fits over shaft between the rolls.

The composition rolls should be changed to the rubber ones whenever found.
Where a stencil developer has been used in a machine, there is most always trouble with the paper
feed caused by the feed rolls getting gummed up, also the paper finger rolls.

The best policy is to change the small feed rolls completely and give the large feed rolls a thorough
cleaning in alcohol, oiling well before replacing in machine and place new paper fingers.

It is a good plan to oil the bearings of the hangers well.
This will keep the paper release working easily.
Too much tension on the feed rolls will cause the paper release to work hard.

On machines with the old style paper table and rest combined, the number of sheets that can be fed
through the cylinder are limited.

Sometimes where a great number of carbon copies are made, it will be found necessary to grind the
feed roll hangers to allow more drop.

Adjust the fingers on paper table to take the extra number of copies.
Watch to see that type bars do not strike on paper table when using shift.

On the ordinary machine the old style paper table should be fitted to take at least three sheets of
paper freely when the paper release lever is thrown.

Try the paper release lever to see that it does not rub anywhere, also the connecting link.

Connecting link is fastened to release crank by screw placed through hole in connecting link.
A round washer is set on before fastening.
The release lever should stay down when depressed and snap back on releasing.

If there is rub or bind it will not snap back. If the rod is bent running under carriage
[(**O\ Plate\ 3**)](#p3) it will cause a sluggish paper release besides holding the feed rolls away
from the cylinder, causing the paper to shift when line space lever is used.

See that the paper release bracket screws are not loose.
Also tighten screw that holds release lever to bracket.

Be careful in tightening this screw as the head will twist off in the paper release crank.
If this happens it can be drilled out.

Adjust paper table [(**E\ Plate\ 3**)](#p3) by taking out any side play in lugs that fit over shaft,
seeing that openings are central with feed rolls.

Place the feed rolls in position, set paper table in place and get the cylinder or platen ready.


## 28. Cylinder or Platen


In placing a new platen in machine usually on the older machines the ends will have to be milled
down with a cylinder end counterbore, until the ends fit in between the bushings without any play.
On the newer machine the cylinders usually fit snugly.

The best way to mill the ends is to place the counterbore in a lathe and run the cylinder ends
against counterbore.
This will insure even trilling.

There should be just the slightest rub where the ends go in the bushings.
If there is side play in the cylinder ends there is a liability to bad side alignment when the
machine is in operation.

In some cases where an old cylinder is ground down to take out depressions made by type it will be
found to have side play if placed in another machine.

In a case like this, washers are put in between the cylinder end and bushing to take out the play.

When placing the cylinder shaft through cylinder pull the ratchet roll [(**H\ Plate\ 9**)](#p9) out
to allow cylinder ratchet to push into place.
Also place ratchet release lever [(**I\ Plate\ 9**)](#p9) over roll.

See that cylinder knob is set so that it holds cup collar close but does not bind the ratchet
release lever [(**I\ Plate\ 9**)](#p9) on the older style machines.

This is tried by turning the cylinder knob towards you.
The ratchet release should remain stationary.

On the latest style machines the ratchet release hub is done away with, also cup collar, and a
bearing that is semi-circular, fits over hub on variable line space drum.

There should be a little play in the ratchet release when lifted with finger.
If this play is not there, there is a liability to hold the ratchet pawl roll [(**H\ Plate\ 9**)](#p9)
away from the ratchet wheel and cause uneven line spacing.

It will also cause the cylinder to turn too freely as the tension spring [(**M\ Plate\ 9**)](#p9)
will have no chance to act.

When the cylinder is placed in machine, if the machine has the old style paper table, adjust for the
number of sheets to go through as instructed under Inner Carriage.


The paper should be absolutely free in the cylinder when the paper release lever is down.
See that all set screws are tight.
The tension on spring [(**M\ Plate\ 9**)](#p9) is adjusted by bending the end that rests against
the pin [(**N\ Plate\ 9**)](#p9).
Be careful in bending spring not to break pin.

See that flat spring [(**L\ Plate\ 9**)](#p9) has enough tension to hold line space adjuster
[(**F\ Plate\ 9**)](#p9) at the different points numbered on top plate.

There should be no bind at screw [(**C\ Plate\ 9**)](#p9) shoulder, the pawl [(**E\ Plate\ 9**)](#p9)
should be perfectly free to work into ratchet tooth.

Also see that there is enough tension on spring that holds pawl [(**E\ Plate\ 9**)](#p9) down.


## 29. Paper Guide and Paper Fingers


Place lateral guide on rod, placing a little tension on spring to hold lateral guide bracket in
place on rod.

This rod is fastened in back of roll on paper rest to two brackets on each end of carriage.
The groove in rod should be set toward the back.
The paper fingers are assembled and placed on the same style rod.

In placing the paper clamps on the rod always add a little tension to the flat springs that hold the
roll in the groove on rod.

In fastening paper fingers to the paper clamps, place the bearing plate in back of the paper finger
and the paper finger in back of the paper clamp and fasten into place by two screws.

After the paper clamps assembled are fitted to carriage, adjust the paper fingers to the cylinder.

The rubber rolls should come down on the cylinder with enough tension to hold paper after it leaves
the feed rolls.

This is adjusted by bending the paper finger in different ways until it has the required tension.

Do not have the ends of the paper fingers bent in so that the paper will roll outside of the fingers.

Set about as shown in [(**Plate\ 3**)](#p3). This is adjusted by holding the paper finger roll
[(**D\ Plate\ 3**)](#p3) and by bending the end of paper finger below the roll.

It is best to put a drop of oil on the paper finger rolls, wiping off any surplus oil.
See that wing scales are bent out so that the paper fingers do not catch on them.


## 30. Ring and Cylinder


Place the skeleton carriage assembled in machine and try the type bars to see that the type hits
the face of cylinder at the same time hitting on ring of segment at [(**J\ Plate\ 2**)](#p2).
This is determined by throwing the carriage pointer to 0 on front scale.

Bring type up into the guide, holding it there by pressing on the key top.

Insert a piece of paper between the type and the cylinder.
The paper when drawn out should be held by the type so that you can feel the drag.

This is also tried at the point [(**J\ Plate\ 2**)](#p2) when the type bar hits on the ring.

The amount of drag should be equal at both places.

If there is a lot of drag on the cylinder and none at the ring [(**J\ Plate\ 2**)](#p2) the cylinder
should be thrown back by adjusting eccentric bushings [(**H\ Plate\ 3**)](#p3) on lift hook shaft,
by turning this bushing, which throws the lift hook forward or back away from the type bar guide.

This is tried again with the carriage pointer around 70 on the front scale, and the amount of drag
made equal in both places.

After the two ends have been evened up try in the center to check up.

After this is completed and the type bars are on ring and cylinder, try the lift hooks for play,
or to see that they don't bind.

In adjusting the bushings [(**H\ Plate\ 3**)](#p3) they may have been moved to the right or left
a little.

If the corners of the skeleton carriage were set before trying the ring and cylinder, changing
the bushings [(**H\ Plate\ 3**)](#p3) will throw the corners off and the link will have to be
readjusted.

Sometimes when the carriage binds in shifting it will be found that the bushings
[(**H\ Plate\ 3**)](#p3) are adjusted incorrectly.


## 31. Setting Corners


When taking the skeleton carriage out or placing same in machine always set the carriage pointer at
10, on No. 5 machine, and also so that the carriage latch [(**A\ Plate\ 3**)](#p3) will not hit on
anything on the longer carriage machines.

This will avoid bending the latch in taking carriage out.
When the carriage is in machine see that the four corners come down on their respective resting
places [(**G\ Plate\ 10**)](#p10) at the same time, when the carriage is shifted up or down.

To test the corners to see if they are off, tap the carriage over the resting place with the finger.
If the corner is down the tap will feel solid and if the corner is off, there will be play between
the piece on carriage and the resting place.
If the shift lock is in a machine it should always be taken out when trying the corners as the shift
lock may be the cause of the corners being off.

When adjusting the corners down adjust by twisting the carriage end until they are down.
Sometimes if may be found that both the corners of one end will not come down on the rests.
In this case the trouble will be found in the lift hooks and the hook on the side that the corners
are off should be bent down, holding the opposite hook with pliers in bending.
All this operation is done with the carriage link unfastened.

When the corners are all down solid fasten the carriage link in place.
This should be set so that there is no bind and is adjusted by bending parallel with carriage end
bearing.

If after the link is on carriage the front or rear corner comes off the rest, the link is shortened
or lengthened by laying it on something flat and hitting with hammer to lengthen it, or placing it
between the jaws of a vice to shorten it, trying in machine until the corner off comes down.

On machines with long carriages there are two carriage links and the left one will have to be
adjusted also if the corner is off on the left end.

If the back corner should be off, the link will have to be lengthened,and shorten if the front
corner is off.

After the corners are all set, try again by tapping, to check up.
To make the adjustment accurate, a piece of paper can be placed between the point on carriage and
the rest and pulled out after letting the carriage down.

There should be a drag to it in pulling.
This tried for the four corners and the drag made equal will insure a perfect job.

See that none of the carriage corners are bent that come down on rests.

Sometimes the back corner pieces get worn and by straightening out they will come down
more solidly.

If the parts on front corners of carriage are t wisted they can be straightened.


## 32. Right Hand Cylinder Knob


Set the right knob in place on cylinder shaft.

On old style machines there is a cup collar that is placed first.
The cylinder knob should be set as close as possible but still leaving the carriage to shift without
a bind.

On the late style machines the knob is set up against [(**B\ Plate\ 10**)](#p10) the set screw
in knob partly tightened and the knob twisted to bring it away a little so that it will not bind
in shifting.

After trying the shift turning the cylinder around to make sure that the knob doesn't bind any
where.

Tighten both set screws as much as possible. Run a piece of paper in between ratchet wheel
[(**J\ Plate\ 9**)](#p9) and any parts in back of it to see that there is no bind at any point.

Sometimes the rachet will hit on end of spacing rack. In this case a washer or two can be placed on
cylinder shaft before it is placed in carriage.


## 33. Motions


The motion is the aligning up of capitals with the small letters, getting them so that the bottoms
of each are on a straight line.

This adjustment is made by setting two motion blocks one each end of the carriage on the inside at
point [(**C~1~\ Plate\ 3**)](#p3).

They are set in an incline, forward or back.
These blocks keep the carriage from shifting too high and control the position of the capital letters.

If the caps should be above the small letters the block is raised or pried away from the inner
carriage with a screw driver.

Set screw must be loosened to move the block.
If the caps run below the small letters the block is lowered or pushed forward by hitting on back
with screw driver.

The motion is always tried at both ends of the carriage and the block on each side adjusted
accordingly.

The motion is tried by stricking the nN key lever alternately vis; nNnNntinNnN".

The carriage should hit on both motion blocks at the same time when shifted.

If to bring the caps on line with small letters the motion blocks have to be pushed so far that they
stick the carriage in shifting, the block can be taken out and the end ground off a little.

In some cases the slot in motion block may need to be filed to make longer so that it will go down
further.
This is in rare cases however.


## 34. Shift Lock Shaft


The shift lock shaft [(**R\ Plate\ 8**)](#p8) is placed on machine with two pin screws and should be
oiled at bearings.
See that no parts of shift locking arm are worn badly.
As shift will lock if piece is worn that hold pin from going under locking lever.


## 35. Key Lever Comb


The comb is set so that the pins on shift key levers rest on top of the shift locking lever
[(**R\ Plate\ 8**)](#p8) throwing arms, leaving no play in each.

This is tried by holding the shift locking levers on shaft [(**R\ Plate\ 8**)](#p8) up against pins
on shift key levers.

Set the comb so that the shift key pins come to the top of the levers about the thickness of the pin
on the left side and so that the right side will not force under the pin.

To adjust the play in this the shift locking lever at the right is bent in or out by laying on
something flat and hitting with hammer.
This is adjusted until there is no play in each shift key lever after the comb is set.


## 36. Shift Lock


Place the spring [(**T\ Plate\ 8**)](#p8) on shift lock hub.

The end of spring should be straightened until parallel in opposite directions before placing.
Hook the spring under the crooked part of shift lock with hook on spring resting underneath crook
with crook down.

The straight end is pushed under casting as the shift lock is placed in machine.
Put the pin in hub pushing way in through casting and fasten set screw [(**S\ Plate\ 8**)](#p8).

Now take the hook on spring [(**T\ Plate\ 8**)](#p8) and force around and hook over back of, and on
top of shift lock.

Try the shift lock to see that it snaps back freely when pulled down from the front end.

Now hold the forward end of shift lock up against the shift lock throwing arm on
[(**R\ Plate\ 8**)](#p8).

Try the two shift keys.
There should be a little play to each. Slightly less than 1/16 of an inch.

This play should be there so that when shift is being used the pins on shift key levers will start
the shift locking arms before the carriage starts to shift.

If there should he no play the shift lock might be held up so that the carriage would not
hold on the lower case when lifted by the cylinder knobs.

If there is too much play the shift key levers will start to raise the carriage before the shift
locking arms are put in action and the shift would start with a decided click and work hard.

There would also be a liability of the carriage dropping away from the motion block on the upper case.
This play is adjusted by bending end of shift lock at crook up or down.
Hold shift lock to space bar shaft when bending up.

The opening on the end of shift lock [(**N\ Plate\ 8**)](#p8) should be adjusted so that the stud on
arm that runs down from lift rail fits in the opening and on trying the shift lock from the front
there should be an equal amount of rub on the upper and lower case in this opening in relation
to the stud.

This is called "Pick". It is adjusted by loosening set screws on shift lock and sliding piece one
way or the other.

To try for this "Pick" have the carriage down and move shift lock from front with finger up and down.
When it is coming down there should be felt a slight rub.
This rub should not be so much that the shift lock has to be forced to bring it down into position.

Set the upper case by pressing shift lock key lever.
Now pull the shift lock down and up.
On going up there should be the same rub or "Pick" as for the lower case.

If anything it should be less than that on the lower case. If the "Pick" is too much on the upper
case the carriage will drop away from the motion blocks.

Also it will probably be found that there is no "Pick" on the lower case. This "Pick" is what holds
the carriage from jumping up and down when in operation.

By lifting on cylinder knobs when carriage is down the carriage should remain there with a little
play in the carriage latch [(**A\ Plate\ 3**)](#p3) in relation to the lift rail.
This play should be as small as possible and is adjusted by bending carriage latch.

Take off the carriage link and start to take inner carriage out before bending latch
[(**A\ Plate\ 3**)](#p3).

On the upper case with shift lock key lever thrown the carriage should remain up to the motion
blocks when tapped with finger on each end.

If it falls away cutting shift lever on bottom will throw it up:
To take the pick off the upper case slide the piece on end of shift lock down a little and tighten
set screw and try the pick.

If the pick is too much on the lower case slide the piece up.
Try the pick each time the sliding piece is adjusted.

If there is no pick on either upper or lower case the end of the lock can be spread by placing
something in groove between openings for upper and lower case and spreading with a hammer blow.

If there should be too much pick on both upper and lower case the parts where the stud rubs can be
filed a little.

Before filing, however, try raising the ends of the shift key throwing levers, by bending up with a
hammer handle placed in at back of machine under the shift key lever throwing hooks.

Try peening the key lever by cutting on the lower edge, this will also throw the lever up.

Do the peening between the key lever comb and space bar shaft [(**C\ Plate\ 8**)](#p8).

Watch the play in the shift key lever in relation to the shift locking release levers on
[(**R\ Plate\ 8**)](#p8) whenever cutting or peening.

See that the shift lock does not bind anywhere by trying up and down with the finger.
If there is any rub on the casting or the lift rail locking arm bend the shift lock out of the way
until free with a bender or three prongs.

Do not throw it over so far that the set screws holding sliding piece rub on key lever or that the
end of shift lock will catch on casting stopping it from dropping back into place.

## 37. Bichrome or Two Color Attachment

The following is adjusted with carriage off.
Attach the bichrome shifting bracket [(**C\ Plate\ 7**)](#p7) to the universal frame.

See that it shifts easily before setting in position.

Next attach the ribbon actuating arm and bracket [(**B\ Plate\ 7**)](#p7) [(**G\ Plate\ 7**)](#p7)
to lift rail with two screws and tighten.

If these screws are not set tight, in operation of the machine they will work loose and the ribbon
will not throw correctly.

The actuating lever should be set so that the oscillating arm goes through the slot in universal
frame without rubbing on either side.
The arm should be central in the universal frame slot and also central with a hole in the segment.

After setting the actuating lever, place ribbon guide or carrier in position.
This is pushed down through the opening between the segment and cross bar, the end is hooked over the
hook on actuating arm.

The end of the ribbon guide when it hooks to the actuating arm should be bent out a little so that
there is a little back play in the slot after the dust plate [(**A\ Plate\ 7**)](#p7) is in place.

After tightening set screws in dust plate [(**A\ Plate\ 7**)](#p7) try the ribbon guide to see that
it is perfectly free.

To try this shift the lift rail to upper case and depress the space bar.
The shifting bracket [(**C\ Plate\ 7**)](#p7) should be pushed over so that the pins on actuating
arm [(**B\ Plate\ 7**)](#p7) are central with bracket and do not rub on either side.

They should be in position shown in [(**B\ Plate\ 7**)](#p7).
The ribbon guide should then be lifted as high as it will go.
It should drop back into place without sticking.

The ears on the ribbon guide that slide along the sides of the type bar guide should be set in as
close as possible allowing the smallest amount of play when tried sideways.

If the ribbon guide should stick when lifted the guide can be forced back a little with the finger
or by giving it a sharper bend at the end where it hooks to actuating arm.

Too sharp a bend will cause ribbon not to throw high enough and it will also jump out of hook at
times during operation of the machine.

Next place the bichrome shift keys. [(**P\ Plate\ 10**)](#p10) through the bichrome shift bracket at
front of machine on right side of casting.

Push shaft through lift rail spring hanger [(**K\ Plate\ 10**)](#p10).
Put collar over end and set up close to lift rail spring hanger, allowing a little play in shaft
when pushed from the front.
This play should be there to allow room for the front name plate to fit in between the
shifting keys and shift key bracket. [(**L\ Plate\ 10**)](#p10)

Attach the connecting link [(**U\ Plate\ 7**)](#p7) from shifting keys shaft to the shifting bracket
[(**C\ Plate\ 7**)](#p7) and fasten with screw to bracket.

Before fastening the set screws to ribbon shift keys shaft, throw the stencil lever
[(**F\ Plate\ 7**)](#p7) over to the left, and set shifting bracket [(**C\ Plate\ 7**)](#p7) so that
the pins come in the center with equal amount of space on each side of pins as shown in\ Plate\ 7.

As the actuating pins are set central. tighten the set screws holding the connecting link
[(**U\ Plate\ 7**)](#p7) to the ribbon shift keys shaft.

The ribbon guide is now tried to see that it jumps high enough, also that it is set so that the
writing will be visible when down.
A ribbon is put in machine and placed in ribbon guide.

The stencil lever is thrown to the left and the carriage shifted.
When the carriage come down the ribbon guide should come down simultaneously.

This is adjusted by a rest for the actuating arm pointing forward from the escapement frame. This
can be bent up or down with a bender.

It should be bent until the ribbon comes to the bottom bevel on the character (/), when it is up in
type bar guide, the space bar should be depress at the same time.

Next throw the shift and lock on upper case, depress the space bar and the character (6) six and
underscore, throwing the type up into type bar guide.

The ribbon should come up to the bottom bevel of the underscore and directly in the center of bevel,
to be set correctly.

The stencil lever is thrown to the left during these tests.

If the ribbon is above or below the bevel on the underscore (-) it is adjusted by bending the lug
[(**V\ Plate\ 7**)](#p7) forward or back as the case may be, the actuating arm rests against this
lug and determines the position of the ribbon guide in relation to setting the pins on actuating
arm in the slots of shifting bracket [(**C\ Plate\ 7**)](#p7).

This lug when bent raises or lowers the ribbon guide and also changes the position of the pins of
actuating arm.

After the ribbon guide is set with the stencil lever thrown over to the left.

Set the shifting bracket [(**C\ Plate\ 7**)](#p7) and adjust the ribbon thrown on the red and black.
This is first tried by loosening the screw in the shifting bracket,
and shifting from black to red with the shift lock down and carriage on upper case.

The shifting bracket is set so that the pins go in the slots of bracket freely without any rub,
the set screws in bracket are then tightened.

If the pins rub on the back of the slots the universal will be thrown away from the segment and
the machine will be thrown into a sluggish action.

Also this would make the ribbon jump to high. If the pins should rub on the front of the slots the
ribbon guide would raise and the writing could not be seen readily. Release the shift lock and
proceed to adjust the slots of the bracket to the pins on the lower case.

Shift the bracket with the red and black key. The pins on the lower case should not rub on the front
or back of skits.

If it is found that they rub, the slots are adjusted to the pins by bending, forward or back
according to where the pins rub.

It is preferable to have the pins just barely rub on the front of the slots.

In some cases it may be necessary to have them rub a little more to make the ribbon jump high enough.
They should never rub on the back of slots however.
After the shifting bracket slots have been adjusted for upper and lower case, check up the upper
case after adjusting the lower to see that it hasn't changed, and then try the ribbon as to its
position in relation to the type.

The ribbon should throw perfectly after these adjustments have been made.

On the black the diagonal (/) is thrown up and the ribbon should come to the top of the bevel.
It does not necessarily have to cover it. This is the longest character on the key board and if the
ribbon covers this it will take all the other characters.

When the shift is used the ribbon should cover the whole face of type.

On trying the red or lower part of the ribbon, on the lower case the ribbon should cover the
whole of the face of the type, and on the upper case the ribbon should jump and cover the upper
half of the type.

It should cover the (—) underscore which is the lowest character on the upper case.
it is best to try these adjustments out with a red and black ribbon.

There is a special jig for bending the actuating arm.
The actuating arm can be bent slightly near the hook to make the ribbon jump higher in some cases.

This, however, is very rarely done. The carriage can be put on and the ribbon tried.
The character used in trying are the Diagonal on the lower case and the underscore (—), and
percentage mark (%) on the upper case.

If they all print on the red and black it will show that the ribbon is correctly adjusted.

If there is a drag to the ribbon in winding, it can be lessoned by bending the pins back on the
ribbon guide where the ribbon comes through, when bending the pins back straighten the little pieces
at top so that the ribbon can be place in guide. Also oil the guiding spools on ribbon spool cup.


## 38. Line Space Mechanism


Some of the adjustments of these parts are mentioned under CYLINDER.

The line space lever is placed on carriage after skeleton carriage is in position in order to adjust
it to the line space plunger [(**G\ Plate\ 9**)](#p9).

Fasten lever into place, setting line space lever pin and tightening set screw so that the line space
lever is free to swing either way without binding. On the older machines the line space lever is
fastened in place with a pin screw, and lever can be filed on the end or bearing point until
free.

When the line space lever is in position there should be a little play between the throwing part of
line space lever and the face of plunger [(**G\ Plate\ 9**)](#p9) on both upper and lower case.

If this play is not there it will cause a sluggish shift. Next pull the line lever over to the right
to see that it pushes the plunger pawl far enough to send the ratchet wheel around to the desired
spacing without any back play.

To determine this set the line space adjuster [(**F\ Plate\ 9**)](#p9) at one space and throw the
line space lever to the right holding it against casting.

Now try turning the left cylinder knob back. It should be held there and not turn back.

If it does the casting should be filed, where the line space lever hits until the back play is taken
out.

Sometimes if there is too much play when the lever is over to the left, the line space lever can be
bent so that it will give it a further throw.

Throw the line space lever over to the right after this is adjusted and hold against casting.

Now try turning the cylinder knob away from you.
There should be just a little play between the plunger pawl and the stop [(**B\ Plate\ 9**)](#p9).

If the stop is too close the plunger will stick and not come back.
If there is too much play the cylinder is liable to race around the desired spacing, or the ratchet
roll [(**H\ Plate\ 9**)](#p9) is liable to locate on the point of a ratchet wheel
[(**J\ Plate\ 9**)](#p9) tooth and in writing a line is liable to jump into place in the middle
of the line.

The stop [(**B\ Plate\ 9**)](#p9) should be bent as close to the pawl [(**E\ Plate\ 9**)](#p9)
as possible without having the plunger stick.

This should be tried by line spacing around the whole number of teeth (33) on the ratchet wheel.

If it is found that the line space lever throws the plunger pawl too far, the line space lever can
be bent or a screw put in the casting for it to bank against until it throws the desired distance.

It generally doesn't throw far enough. See that all screws are tight in the plunger and parts on the
line space mechanism.

See that there is not too much play in the line space plunger [(**G\ Plate\ 9**)](#p9) in the slot
it works through.
To do away with the side play a piece can be soldered to the carriage frame along side of the plunger
on the right side.

If plunger gets broken a new one can be fitted. There is a small set screw with a washer that goes
in plunger which holds the plunger from coming out too far.

If this screw comes out, the plunger would come back a little too far and the plunger pawl would
drop below its carrier and the line spacing would fail to work.

This would also take the play out from between the line space lever and plunger, causing a sluggish
shift.

The screw in the end of the line space adjuster often comes out, and causes the shift to stick
making the caps jump around.

An ordinary scale screw can be put in place, filing the end of screw when tightened in place to
make it nearly flush with carriage end then it can be riveted into place so that it will not work
out again.

In fitting a new ratchet roll [(**H\ Plate\ 9**)](#p9) on the arm at [(**K\ Plate\ 9**)](#p9)
there is a cup collar that has to be fitted as the arm must be free when the screw
[(**K\ Plate\ 9**)](#p9) is tight.

This is adjusted by filing the cup down until the play is there to make arm free.

In fitting a ratchet wheel other than with the standard number of teeth the plunger pawl
([(**E\ Plate\ 9**)](#p9) ratchet roll [(**H\ Plate\ 9**)](#p9) ratchet wheel [(**J\ Plate\ 9**)](#p9)
and adjuster back of [(**L\ Plate\ 9**)](#p9) will have to be changed.

Also the line space lever throw will be changed.

After changes are made try play in ratchet release [(**L\ Plate\ 9**)](#p9) to see that ratchet roll
[(**H\ Plate\ 9**)](#p9) is not held away from ratchet wheel teeth [(**J\ Plate\ 9**)](#p9).


## 39. Variable Line Space Drum


The variable line space drum [(**J\ Plate\ 9**)](#p9) is fitted to the cylinder shaft and assembled.
Under the plate are two locking levers, two large and one small pin, also the two friction
brakes.

These are held in place by the top plate. There is no adjusting to these except in spreading the
brake pieces which is done by tightening set screws [(**A\ Plate\ 9**)](#p9).

Sometimes when a condensed biller is put on a machine the variable line spacer is locked by placing
a long screw in place of [(**A\ Plate\ 9**)](#p9) with hole and putting in a beveled head screw from
the back side of drum.

The variable line space knob is set on with cup, in the cup goes a round collar and a third pin is
set in which grips the edge of cup.

These are placed on the cylinder shaft and the set screw tightened so that there is a little play to
the nickeled knob, which has a groove in one of the pins and is held from working out by a springed
piece on cylinder knob.

The cup should push into the opening in variable line space drum with a decided click.

If this click is not there the cylinder knob will turn when the line space lever is thrown over to
the right and the friction brakes will not hold.

This is adjusted by tightening screw [(**A\ Plate\ 9**)](#p9).
Tighten set screw after turning the screw [(**A\ Plate\ 9**)](#p9).
See that ratchet wheel is tight on cylinder rod.


## 40. Tabular Rack


Set the tabular rack in position placing washer over pin at right end (facing the back of machine).

Push fulcrum pin in from left and take out all side shake to rack and fasten set screws
[(**D~1~\ Plate\ 5**)](#p5).

Do not unscrew the pin at right so far that the washer will rest against the casting.

Always have it bear on the shoulder of the pin. This pin adjusts the rack so that it will stop one
space before the one wanted, before the tabular key lever is released.

Fasten the connecting link [(**F~1~\ Plate\ 5**)](#p5) in position.
At the bottom there is a lug on the tabular key lever that the connecting link is hooked to and it
is fastened at the top to the tabular frame with a screw with shoulder.

After fastening in place, press down the tabular key lever. The carriage should run along with
uniform speed.

Set stops at short intervals to try this out.
If the carriage bangs from one stop to another, the connecting link can be shortened or perhaps the
rack isn't pulling forward enough.

When depressing the tabular key, the spacing rack release lever should come down at the
same time that the tabular key hits bottom. If there is playing the spacing rack release lever when
the tabular key is down the play is taken out by bending the lug [(**W\ Plate\ 7**)](#p7) down.


If the spacing rack lever comes down before the tabular key reaches bottom the lug is bent up.

If it is found that the carriage runs too slow when the tabular key is used the rack can be forced
out with the hand by pushing back, stretching the connecting link or shifting the connecting link
fastening lug.

If the carriage should run faster at one end than it does the opposite, the adjustment can be made
by two set screws [(**V~1~\ Plate\ 7**)](#p7).
These throw the rack out or in.

After getting the carriage to run along evenly and fairly fast, see that the pin on tabular key
lever does not force by the stop on key lever comb.

This stop can be bent in or out.

Ball bearings sometimes rust in the tabular stops. When this occurs the tab stop will drop out of
the rack teeth and fail to stop the carriage.

To remedy this the tabular rack must be taken off.
The end of rack removed, and the stop taken off.
Place oil in the hole where ball bearing is located and work ball up and down with a match.

Be careful not to let ball bearing spring out.

If this dues not free it, take the ball out and clean off and replace.
It is a bothersome job setting these stops on the rack and care must be taken.

Push the spring in place and set ball at top forcing in hole with match.

Place over rod on rack before pulling match out. These ball bearings snap into grooves on tabular
frame and keep the stops in the teeth of rack.


## 41. Wing Scales


Type about 15 m's viz., mmmmmmmmmm and adjust lines on wing scale to middle line of m and have
scale rest on line with bottom of letter.

Have scale set about 1/32 of inch away from platen by adjusting lug in back of wing scale that
rests on dust plate [(**A\ Plate\ 7**)](#p7).
Tighten screws.


## 42. Alignment of Type


To try the alignment the letter nN is used as a base on all machines.

All the letters should be tried alternately 

viz N"N#N$NGAN—N\& nanbncndnenfn NANBNCNDNENF;-N"N\#N$N%N-N\& etc.

All the letters and characters should be on a straight line, not bent over, or off their feet,
that is, print lighter on one side than the other.

See that the motion is perfect before attempting to align.
Align the nNn first. Getting the capital directly in the center between the two small n's without it
being on a slant.

Side aligning pliers are used to throw the type to one side if the cap is on the slant when central.
To throw the type up or down the type bar is peened as shown in [(**Plate 6**)](#p6), here the type is
being peened to lower it.

To adjust the capital the type is bent over one side or the other to make it come central between
two small letters.

If on certain type it is found that the motion is out, the type can be cut in the middle to spread
the cap from the small letter. However, if the motion is out caused by the type already being cut,
solder on a new type.

If the type prints lighter on the top than on the bottom the upper part can be thrown out by cutting.

If the type prints lighter on one side than on the other, twist the type until square.
After the type has been aligned see that all type are solid on the bars and go over the type bars
trying them to see that they are still free in the type bar guide.

When soldering on new type use a gauge to have it the same as others, so that it will be on ring
and cylinder when in place.


## 43. Finishing Up


In finishing up the machine, place the top plates in position, left spacing rack release, seeing that
it does not rub on top plate or variable line space drum.

Place front plate in position, touch up the main frame where there is any enamel knocked off or worn
down.

Polish the enamel up to a good lustre and the machine is finished.

**Note**:
After finishing the machine it is a good plan to go over the main adjustments to see that they are
correct before putting machine away, such as trying backspace, shift lock, tabulator, throw of line
space lever, paper release, play in lift rail, play in tabular rack, play in rocker arm, play in
dogs, play in carriage latch by lifting cylinder knobs, to see if shift lock holds, trip, tension on
carriage, try the set screws in escapement, cylinder screws, knob screws play in locking piece on
carriage ribbon shift ribbon pawls and give the machine a general looking over.

This is to make sure that something was not left loose or forgotten.

In changing sets of type, the segment should also be changed and the one coreing with type used.

If other than standard spacing between letters is to be used the changes to make are the spacing
rack, tabulator, pinion wheel and star wheel, marginal stops, rear rod [(**P\ Plate\ 7**)](#p7)
scale, locking piece on carriage [(**B\ Plate\ 3**)](#p3) and bell trip rack [(**R\ Plate\ 7**)](#p7).

In regards to the very old machines there are so few of them that they are not taken up in this book
completely.

Adjustments given, cover the largest part of workings of all the later machines and can be applied
to the older machines as the principal is about the same.


## 44. Machines for Stencil Cutting


A machine that is to be used for stencil cutting should be gone over specially to adjust it for
cutting stencil.

As compared with ordinary writing we have a thin tissue to cut through which requires that the type
break a clear impression, while in ordinary writing a clear impression is essential but adjustment
can be out a little and still there will be a seemingly clear impression.

The main adjustments to look out for when adjusting for stencil cutting are :
To have the type bars on ring and cylinder at each end of carriage and in center.

A good, smooth, fairly hard, cylinder should be placed on machine with every bit of end play at
bearings taken out.

The cylinder must be perfectly true. Take out all side play in dogs, universal ears, lift rail,
carriage lift hooks and carriage lift hook bushings.

The type bar guide should be closed up as tight as possible, seeing that tongue on type bar is
parallel with opening in guide, and that the minimum amount of play is in type bars when in guide.

See that the type bar action is absolutely free, that the ribbon pawls can be thrown away from
ratchet wheel and that stencil lever can be thrown to left without jamming.

The ribbon guide should drop low enough so that no part of type can touch ribbon. See that pins on
actuating lever arm are central in shifting, bracket so that ribbon guide will remain stationary
when key levers are depressed. The main and most important adjustment, see that the type hit
the platen or cylinder squarely, and that when trying out with the lightest inked ribbon obtainable

(one that has the ink mostly worn off from use is the best to try with) a perfect impression should
show.

Sometimes it requires a good deal of cutting of the type to throw the top or bottom out to make
an even impression.

Adjust the shift lock correctly and see that four corners of skeleton carriage come down on their
resting places simultaneously and remain there. Take all excess play out of carriage latch over lift
rail and see that motion is set correctly. Particular attention should be paid to the capitals of
characters 0, A, W, M and P.

It is best to try out the machine on a stencil before leaving it.


# QUICK REFERENCE TO LOCATING TROUBLE

**Note**:
The following notes and questions are merely an aid in locating where trouble can be found with the
most common troubles found in Underwood Typewriter Repairing.

To adjust correctly, follow the foregoing instructions listed in contents under title of
part affected.


## 01. Paper Feed


Are feed rolls stuck or rusted? Oil feed rolls on shaft and clean thoroughly with alcohol.

Are slots in paper table [(**E\ Plate\ 3**)](#p3) central with feed rolls?

Is the tension on feed roll hanger springs [(**Q\ Plate\ 3**)](#p3) uneven?

Is paper release lever binding?

Is feed roll release rod [(**O\ Plate\ 3**)](#p3) binding?

Do the feed rolls snap back against the cylinder perfectly free?

Are the paper finger rolls [(**D\ Plate\ 3**)](#p3) rusted or sticking?

See that there is enough, but not too much tension on paper fingers.

Is there too much tension on paper clips [(**P\ Plate\ 3**)](#p3)?


## 02. Irregular Spacing Between Lines


Are the cylinder set screws loose?

On late machines, does the variable line space knob [(**R\ Plate\ 3**)](#p3) push in place without a
decided click? If not, tighten screw [(**A\ Plate\ 9**)](#p9) two or three turns.

Is line space plunger stop [(**B\ Plate\ 9**)](#p9) set so there is play when trying cylinder after
holding line space lever over? There should he a minimum of play to this.

Is the rubber on cylinder loose on core?

Is ratchet [(**J\ Plate\ 9**)](#p9) loose on shaft?

Is cylinder untrue?


## 03. Line Space Lever Sticking


Does plunger pawl [(**E\ Plate\ 9**)](#p9) catch on carrier returning?

Is stop [(**B\ Plate\ 9**)](#p9) bent too far forward?

Does the line space lever bind on top plate?

Is the screw [(**C\ Plate\ 9**)](#p9) loose?

Is plunger spring [(**D\ Plate\ 9**)](#p9) broken?

Is the plunger pawl [(**E\ Plate\ 9**)](#p9) riding over lug?

Is the line space lever pin [(**A\ Plate\ 10**)](#p10) rusted, or binding?


## 04. Letters Running Down Hill


Caused by line space lever throwing too far and ratchet pawl landing on top of ratchet tooth,
jumping back when machine is in operation.

Is the line space plunger stop [(**B\ Plate\ 9**)](#p9) bent forward enough to take out back play?

Is there any forward play when holding line space lever over and turning forward on cylinder knob?
There should he none.

Is ratchet release [(**I\ Plate\ 9**)](#p9) up?


## 05. Letters Drop After Caps


Is the screw missing from end of line space adjuster [(**F\ Plate\ 9**)](#p9)?

Is the right hand cylinder knob [(**S\ Plate\ 3**)](#p3) too close to guide, binding at certain
places on turning cylinder?

Are the motion blocks binding on inner carriage in shifting?

Does the carriage bind in shifting?


## 06. Carriage Skipping


Is there 1/8 inch drop to space bar after escapement trips?

Are the dogs too far apart?

Is the spacing rack set too high in pinion wheel?

Are the spacing rack teeth worn?

Are any teeth on pinion wheel broken?

Are pinion wheel pawl springs broken?

Is loose dog pin loose?

Is there too much hack play when pushing end of dogs [(**HH\ Plate\ 5**)](#p5) when key lever is
pressed down?

Is there too much tension on main spring?

Is eccentric stud set too tight binding tabular block [(**Z\ Plate\ 5**)](#p5)?


## 07. Carriage Failing to Space Properly


Is way rod properly oiled?

Is the trip set correctly?

Are the assembled dogs set too far to the right?
[(**Plate\ 5**)](#p5) shows correct position.
Set by loosening fulcrum pin set screws and turn fulcrum screw on left side [(**Q\ Plate\ 5**)](#p5)
the right amount.

Is the back space pawl [(**T\ Plate\ 5**)](#p5) catching in the spacing rack?
Adjust according to instructions.

Does carriage rub on ribbon spools? File casting on carriage if it should.

Has space bar the full drop after tripping?

Is the pinion wheel or spacing rack roll binding?

Is the star wheel binding?

Is there play between locking piece on carriage [(**B\ Plate\ 3**)](#p3) and scale plate
[(**B~1~\ Plate\ 10**)](#p10)?

Is carriage latch [(**A\ Plate\ 3**)](#p3) binding on lift rail so that carriage drags?

Does carriage roll turn freely?

Is the ribbon guide dust plate [(**A\ Plate\ 7**)](#p7) out of place?

Is the main spring draw band [(**N\ Plate\ 5**)](#p5) rubbing on lift rail in turning?

Is spacing rack fulcrum screw [(**T\ Plate\ 3**)](#p3) out of place?

Are the way rod screws loose?

Do paper fingers catch on wing scales?

Is the carriage frame broken?

Are screws in way rod bushings [(**U\ Plate\ 3**)](#p3) loose?

Is screw [(**AA\ Plate\ 5**)](#p5) loose?

Is loose dog stuck?

Is there enough tension on loose dog spring?

Is main spring or drawn band broken?

Is rocker arm [(**Y\ Plate\ 5**)](#p5) loose?

Is carriage latch [(**A\ Plate\ 3**)](#p3) over left rail?

Are assembled dogs loose?

Is bell ringer held down by clamp?

is loose dog pin loose?

Does carriage bind on left marginal stop?


## 08. Carriage Does Not Lock at End of Line Properly


Adjust according to instructions.

Is bell ringing lever throwing locking rod forward at right position?

Is bell ringer held down by fastening clamp?


## 09. Carriage Spaces When Shifting


Are spacing rack teeth worn? Are carriage top plates rubbing on left or right rack release levers?

Is variable line space drum hitting spacing rack release lever?

Is there too much play between locking piece on carriage [(**B\ Plate\ 3**)](#p3) and scale plate?

Are pins in actuating lever [(**B\ Plate\ 7**)](#p7) set too far to, one side in shifting bracket
[(**C\ Plate\ 7**)](#p7)?

Is there too little tension on spacing rack release lever spring [(**M\ Plate\ 3**)](#p3)?


## 10. Carriage Binds in Shifting


Is carriage link [(**V\ Plate\ 3**)](#p3) bent or binding?

Is ribbon winding wheel [(**K\ Plate\ 4**)](#p4) set in too far, so that it rubs on shift key lever?

Is right cylinder knob set in too close?

Are lift hook bushings [(**H\ Plate\ 3**)](#p3) too tight?

Does carriage bind on carriage guide? [(**B\ Plate\ 10**)](#p10)?

Is shift lock [(**A\ Plate\ 8**)](#p8) binding?

Are shift key levers binding?

Are lift hooks gripping carriage too tightly?

Does carriage link bind on retainer spring [(**N\ Plate\ 3**)](#p3)?

Are motion blocks rubbing on skeleton carriage?

Is line space lever binding on plunger at [(**G\ Plate\ 9**)](#p9)?

Does left spacing rack release lever bind on top plate?

Does the right release lever bind anywhere?

Are the pins in actuating lever [(**B\ Plate\ 7**)](#p7) set correctly?

Does actuating lever rub on universal [(**D\ Plate\ 7**)](#p7) ?


## 11. Shift Lock Does Not Hold


Is comb set low enough to allow shift key to go down far enough?

Are pins or lever worn?

On late style shift lock keys, is the round spring broken [(**E\ Plate\ 7**)](#p7)?

Is the "pick" or rub on lower and upper case unequal when trying shift
lock [(**A\ Plate\ 8**)](#p8)?


## 12. Action Heavy


Are key levers binding any?

Are link studs [(**E\ Plate\ 2**)](#p2) central with slots in segment?

Are any type bars binding or sticking in guide?

Is tension on universal spring too heavy?

Is universal perfectly free?

Is tension on key lever springs [(**L\ Plate\ 2**)](#p2) as light as possible without letting
key levers drop away from comb?

Are fulcrum pins on rocker arm [(**BB\ Plate\ 5**)](#p5) too tight?

Is the ribbon pawl throwing lever [(**F\ Plate\ 5**)](#p5) binding on rocker arm?

Do the ribbon spool carriers work sluggishly?

Is space bar binding?

Is there too much tension on space bar spring [(**C\ Plate\ 8**)](#p8)?

Its space bar throwing arm [(**D\ Plate\ 8**)](#p8) rubbing on key lever?


## 13. Space Bar Sticking


Is universal binding? Are fulcrum screws [(**D\ Plate\ 10**)](#p10) rusted?

Does space bar stop [(**E\ Plate\ 8**)](#p8) rub on main frame?

Is the tension on spring [(**C\ Plate\ 8**)](#p8) too light?

Does space bar arm [(**D\ Plate\ 8**)](#p8) rub on key levers?


## 14. Ribbon Guide Does Not Act


Is stencil lever [(**F\ Plate\ 7**)](#p7) thrown to left?

Is ribbon guide over hook on actuating lever [(**B\ Plate\ 7**)](#p7)?

Are screws loose in actuating lever bracket [(**G\ Plate\ 7**)](#p7)?

Are rocker arm fulcrum pins [(**BB\ Plate\ 5**)](#p5) loose?

Are bichrome shifting bracket screws [(**C\ Plate\ 7**)](#p7) loose?


## 15. Bichrome Mixing Colors


Adjust as per instructions.
The throw of ribbon guide is too high or too low. Are actuating lever [(**G\ Plate\ 7**)](#p7)
screws loose?

Are shifting bracket [(**C\ Plate\ 7**)](#p7) screws loose?

Is ribbon guide filled with dirt? Is universal [(**D\ Plate\ 7**)](#p7) loose or
broken?


## 16. Ribbon Does Not Move


Is ribbon pawl's set screw loose?

Are ribbon pawls away from ratchet wheel [(**K\ Plate\ 4**)](#p4)?

Are spool carriers binding?

Is ribbon pawl throwing arm [(**F\ Plate\ 5**)](#p5) set screw loose?

Are gears [(**D\ Plate\ 4**)](#p4) loose? Are any set screws of ribbon
movement loose?


## 17. Ribbon Does Not Reverse Automatically


Is there enough tension on shaft pawl spring [(**A\ Plate\ 4**)](#p4)?

Is there too much up shake to pawl [(**B\ Plate\ 4**)](#p4)?

Are detent [(**C\ Plate\ 4**)](#p4) and gears [(**D\ Plate\ 4**)](#p4) set incorrectly?

Are shifting wheels [(**E\ Plate\ 4**)](#p4) set away from
shoulder on shaft?

Are any set screws loose?

Is the eyelet missing on ribbon to throw lever [(**H\ Plate\ 4**)](#p4) out?

Are ribbon pawls set too close together in ratchet winding wheel teeth?

Is the spring on ribbon pawls broken?

Is the tension on detent [(**F\ Plate\ 4**)](#p4) too strong?

Is screw [(**G\ Plate\ 4**)](#p4) set in or out too far?

Do throwing levers [(**H\ Plate\ 4**)](#p4) bind?

Do the disengaging pawls [(**J\ Plate\ 4**)](#p4) pull out when tried?

Are the bearings dry? Oil well.

Is collar [(**M\ Plate\ 4**)](#p4) set so that tension is too light on spring?


## 18. To Take Out Type Bars


Pull [(**E\ Plate\ 5**)](#p5) to one side and push in on [(**Y\ Plate\ 5**)](#p5) as far as
possible and hold.

Pull type bar one inch above type bar rest and work up and down, pulling at same time.


## 19. Type Bars Sticking


Is there dirt in segment slots?

Are link studs [(**E\ Plate\ 2**)](#p2) set central with segment slots?

Are there burrs on lip [(**X~3~\ Plate\ 2**)](#p2)?

Are the type bars rubbing on side of guide?

Is there a screw or paper clip under key lever, holding it down?

Are the type bars binding in segment slot?

Is key lever sticking?

Is link binding.


## 20. Spacing Between Letters Uneven


Is there side play to carriage in lift hooks [(**F\ Plate\ 10**)](#p10)?

Is there play in lift hook bushing?

Are the sides of type bar guides too far, apart?

Is the right cylinder knob too far away from carriage guide [(**B\ Plate\ 10**)](#p10)?

Is there end play in cylinder?

Is the typewriter desk or stand loose or wobbly?

Is the spacing rack set on carriage frame with too much end play?

Take out all play in dogs and rocker arm of escapement.


## 21. To Stop Squeaks


Oil arm on space bar at point below [(**F\ Plate\ 5**)](#p5).

Oil places where carriage corners rest [(**G\ Plate\ 10**)](#p10).

Is there side play in lift hooks?
This will cause squeaks if rusty and dry.

Oil motion block stop on inner carriage.

Do not let wing scales rub on paper.

Oil shift lock throwing levers and pins.


## 22. To Stop Rattle


Is paper table [(**E\ Plate\ 3**)](#p3) set with too much end play?

Is there end play in carriage lift hooks [(**F\ Plate\ 10**)](#p10) and bushings
[(**H\ Plate\ 3**)](#p3)?

Are the rubbers [(**F\ Plate\ 8**)](#p8) off of space bar?

Are the rubbers off of paper table [(**W\ Plate\ 3**)](#p3)?

Is the desk or stand shaky?

Are paper rest, top plate or other screws loose?

Take out all play not necessary in any or all bearings.


## 23. Alignment Bad


Is the carriage off corners?

Has the shift lock the right amount of pick on upper and lower case?

Is type bar guide [(**H\ Plate\ 7**)](#p7) open too far, allowing too much side play in type bars?

Do the type bars hit on sides of guide going in?

Are the type too high or low on bars? Is there end play in carriage, lift hooks or cylinder?

Is there too much side play in rocker arm [(**Y\ Plate\ 5**)](#p5)?

Is there side play in dogs?

Is there side play in escapement wheel?

Are the type bars hitting on ring [(**J\ Plate\ 2**)](#p2) and not on cylinder at either or
both ends of carriage?

Are any type bars broken or type unsoldered?

Is type bar worn bad at [(**X~3~\ Plate\ 2**)](#p2)?


## 24. Carriage Pointer Going Beyond One Space When Carriage Is Pulled Over


Does the pointer go father than 3/4 the way into the next space?
If so, place washers on front rod at [(**I\ Plate\ 7**)](#p7)?

Are front rod nuts loose?


## 25. Pointer Drops Back One Space


Is wheel check away from tooth on older machines?

Is spring broken?

Is wheel check worn?

Is wheel check [(**D\ Plate\ 5**)](#p5) spring out of position?

Are dogs set in correct position? Is pinion wheel tooth broken?


## 26. Back Space Doesn't Work


Is there too much up play in key lever?

Is there a pencil or rubber under lever beneath machine?

Is the back space pawl projecting too far out of spacing rack teeth, when it
starts its side motion?

Is the eccentric pin [(**S\ Plate\ 5**)](#p5) adjusted correctly?

Is spring [(**C\ Plate\ 5**)](#p5) away from piece [(**CC\ Plate\ 5**)](#p5)?
Sometimes prying pawl out to make it go further into rack will adjust trouble.

Bending whole back spacer one way or other by pushing with screw driver
[(**X\ Plate\ 5**)](#p5).

Loosen screw [(**R\ Plate\ 5**)](#p5) before forcing out.
To bend other way, place screw driver in screw head that holds spring [(**C\ Plate\ 5**)](#p5) and
hit with hammer.

Is collar [(**Q\ Plate\ 5**)](#p5) away from back space hub?

Does back space wheel check [(**M\ Plate\ 5**)](#p5) hit tooth or go over instead of under?

Is there too much back play in star wheel when wheel check is under tooth of star wheel?

Are screws [(**H\ Plate\ 8**)](#p8) loose? Has point [(**I\ Plate\ 8**)](#p8) become loose?


## 27. Key Levers Lock


Is rod [(**W\ Plate\ 2**)](#p2) binding?

Does hub [(**G\ Plate\ 8**)](#p8) bind or rub on worn rubber foot?

Is there any other than Underwood fastening screw holding machine to desk?

Is bell ringer held down by this?

Is there no side play in bell trip rack [(**R\ Plate\ 7**)](#p7)?

Is there too little tension on locking rod spring back of [(**G\ Plate\ 8**)](#p8)?

Are lock not working at end of line four spaces from end?


## 28. Paper Release Lever Works Hard


Is there too much tension on feed roll hangers [(**Q\ Plate\ 3**)](#p3)?
Oil hanger joints thoroughly.

Is paper release lever or connecting link binding?

Are screws loose in release lever bracket?


## 29. Tabulator Out of Order


When pressing tabulator key, does right spacing rack release lever come down at same time?

Tabulator key hits stop and not before.

Is there play in release lever when tabulator key is down?
There should be none.

Does the brake leather [(**X\ Plate\ 3**)](#p3) hold tabulator from going too fast or
slow?

&nbsp;

To adjust all the above questions properly, refer to complete instructions.

**Write Us About Special Tools for Bending Parts or Aligning Type Bars.**


# PICTURES / PLATES

[]{#p1}

## Plate 1: Frontpiece

![](plate_01.png)


[]{#p2}

## Plate 2: Type Bar Action

![](plate_02.png)


**A\ Plate\ 2**: Key lever

**B\ Plate\ 2**: Key button

**C\ Plate\ 2**: Key lever link

**D\ Plate\ 2**: An adjustment point.

**E\ Plate\ 2**: Link stud

**F\ Plate\ 2**: The fulcrum wire

**G\ Plate\ 2**: Shot pad

**H\ Plate\ 2**: Typeface

**J\ Plate\ 2**: The ring

**K\ Plate\ 2**: Guide

**L\ Plate\ 2**: Key lever tension springs

**M\ Plate\ 2**: 3 Screws (hold key lever tension screws retaining plate)

**N\ Plate\ 2**: Key lever tension screw

**O\ Plate\ 2**: Key lever retaining plate under machine

**P\ Plate\ 2**: Key lever to key link stud (*undocumented*)

**Q\ Plate\ 2**: Hook (*undocumented*)

**R\ Plate\ 2**: Key lever tension screws retaining plate

**S\ Plate\ 2**: Slot of comb at front of machine

**T\ Plate\ 2**: Set screw

**W\ Plate\ 2**: Line lock rod

**V\ Plate\ 2**: Type bars

**X\ Plate\ 2**: Adjustment point

**X~1~\ Plate\ 2**: Adjustment point

**X~2~\ Plate\ 2**: Adjustment point

**X~3~\ Plate\ 2**: Adjustment point

**Y\ Plate\ 2**: Adjusting screws in the segment


[]{#p3}

## Plate 3: Skeleton Carriage and Carriage Frame Assembled

![](plate_03.png)


**A\ Plate\ 3**: Carriage Latch

**A~1~\ Plate\ 3**: Collar (regulates carrige lift spring)

**B\ Plate\ 3**: Carriage Locking Piece

**B~1~\ Plate\ 3**: Collar (sets rear of large feed rolls on later machines)

**C\ Plate\ 3**: (*undocumented*)

**C~1~\ Plate\ 3**: Adjustement points for motion shift mechanism

**D\ Plate\ 3**: Paper Finger Rolls

**E\ Plate\ 3**: Paper Table

**F\ Plate\ 3**: (*undocumented*)

**G\ Plate\ 3**: Tension Spring (to help carriage lifting)

**H\ Plate\ 3**: Eccentric bushings

**J\ Plate\ 3**: Screw (for right margin release)

**K\ Plate\ 3**: (*undocumented*)

**M\ Plate\ 3**: Spacing Rack Release Lever Spring

**N\ Plate\ 3**: Retainer Spring

**O\ Plate\ 3**: Feed Roll Release Rod

**P\ Plate\ 3**: Paper Clips

**Q\ Plate\ 3**: Feed Roll Hangers

**R\ Plate\ 3**: Variable Line Space Knob (late machines)

**S\ Plate\ 3**: Right Hand Cylinder Knob

**T\ Plate\ 3**: Spacing Rack Fulcrum Screw

**U\ Plate\ 3**: Carriage Bushing

**W\ Plate\ 3**: Paper Table

**V\ Plate\ 3**: Carriage Link

**X\ Plate\ 3**: Brake Leather

**Y\ Plate\ 3**: Concave Head Screws (that fasten carriage locking piece)

**Z\ Plate\ 3**: Locking piece


[]{#p4}

## Plate 4: Automatic Ribbon Movement

![](plate_04.png)

**A\ Plate\ 4**: Frictional shaft pawl spring

**B\ Plate\ 4**: Shifting Pawls

**C\ Plate\ 4**: Detent Hub

**D\ Plate\ 4**: Bevel gears

**E\ Plate\ 4**: Shifting Wheels

**F\ Plate\ 4**: Detent Lever Spring

**G\ Plate\ 4**: Detent Lever Adjusting Screw

**H\ Plate\ 4**: Throw Lever

**J\ Plate\ 4**: Disengaging Pawls 

**K\ Plate\ 4**: Ratchet Winding Wheel

**L\ Plate\ 4**: (*undocumented*)

**M\ Plate\ 4**: Collar (which should, be set so that when the throw lever it will spring back into place).

**N\ Plate\ 4**: (*undocumented*)

**O\ Plate\ 4**: Upright Shafts

**P\ Plate\ 4**: Winding Shaft

**Q\ Plate\ 4**: Detent Lever Point

[]{#p5}

## Plate 5: Escapement And Main Spring Drum Assembled

![](plate_05.png)

**A\ Plate\ 5**: Ratchet Wheel Release Handle

**B\ Plate\ 5**: Nut On Star Wheel

**C\ Plate\ 5**: Screw (holds spring to back spacer)

**D\ Plate\ 5**: Wheel check (dog that doesn't let wheel go back, on newer models)

**E\ Plate\ 5**: Service Lever (allows universal to retreat futher so that you can remove typebars)

**F\ Plate\ 5**: Ribbon Pawl Throwing Arm

**F~1~\ Plate\ 5**: Tabular Rack Connecting Link

**G~1~\ Plate\ 5**: Bolts (hold mainspring assembly) (*undocumented*)

**H\ Plate\ 5**: Screw (holds the rigid dog in place in relation to a tooth on star wheel)

**J\ Plate\ 5**: Screw (adjusts position of the loose dog).

**L\ Plate\ 5**: Fastening Pin On The Loose Dog

**M\ Plate\ 5**: The Back Space Wheel Check

**N\ Plate\ 5**: Mainspring Drum

**O\ Plate\ 5**: Screw Holding Handle

**P\ Plate\ 5**: Mainspring Assembly

**Q\ Plate\ 5**: Dog Fulcrum Pin

**R\ Plate\ 5**: Screw

**S\ Plate\ 5**: Backspace Eccentric Pin (adjusts backspace wheel check)

**T\ Plate\ 5**: Backspace Pawl Carrier

**U\ Plate\ 5**: Escapement Frame

**V\ Plate\ 5**: Backspace Connecting Link

**W\ Plate\ 5**: Loose Dog Spring

**X\ Plate\ 5**: Backspacer sub assembly

**Y\ Plate\ 5**: Rocker Arm (rotates pin that moves ribbon)

**Z\ Plate\ 5**: Screw In Tabular Block (adjusts rack pinion).

**AA\ Plate\ 5**: Assembled Dogs Fulcrum Set Screws

**BB\ Plate\ 5**: Fulcrum Pins On Rocker Arm

**CC\ Plate\ 5**: (*undocumented*)

**DD\ Plate\ 5**: Base Of The Escapement Assembly

**EE\ Plate\ 5**: Ears Of The Universal Frame

**FF\ Plate\ 5**: Pin Shaft Of The Universal Frame

**GG\ Plate\ 5**: Trip Nut (triggers the escapement)

**HH\ Plate\ 5**: Screw

[]{#p6}

## Plate 6: Peening The Type Bar

![](plate_06.png)

[]{#p7}

## Plate 7: Looking Into Machine, Carriage Off

![](plate_07.png)

**A\ Plate\ 7**: Ribbon Guide Dust Plate

**B\ Plate\ 7**: Ribbon Guide Actuating Lever

**C\ Plate\ 7**: Bichrome Shifting Bracket

**D\ Plate\ 7**: Frame (holds the universal bar)

**E\ Plate\ 7**: Round Spring (related to shift lock)

**F\ Plate\ 7**: Stencil Lever

**G\ Plate\ 7**: Shift Ribbon Guide Actuating Lever Bracket

**H\ Plate\ 7**: Type Bar Guide

**I\ Plate\ 7**: Front Rod (margins)

**J\ Plate\ 7**: Bell

**K\ Plate\ 7**: Ribbon Pawls And Shaft Assembled

**L\ Plate\ 7**: Screws (holding the typebar rest)

**M\ Plate\ 7**: Bend Point (on universal bar)

**N\ Plate\ 7**: Sliding Piece (on universal frame)

**O\ Plate\ 7**: Lift Rail

**P\ Plate\ 7**: Back Rod (margins)

**Q\ Plate\ 7**: (*undocumented*)

**R\ Plate\ 7**: Bell Trip Rack

**S\ Plate\ 7**: Screws (attach front scale plate)

**T\ Plate\ 7**: Carriage Stop

**U\ Plate\ 7**: Connecting Link (from shifting keys shaft to the shifting bracket)

**V\ Plate\ 7**: Bichrome Adjustemnt Lug

**V~1~\ Plate\ 7**: Tabular Rack Speed Adjustement Screws

**W\ Plate\ 7**: Spacing Rack Release Adjustment Lug

[]{#p8}

## Plate 8: Working Parts Underneath Machine

![](plate_08.png)

**A\ Plate\ 8**: Shift Lock Lever

**B\ Plate\ 8**: Key Lever Tensions Screws (*undocumented*)

**C\ Plate\ 8**: Space bar Shaft

**D\ Plate\ 8**: Space bar Throwing Arm (pushes on escapement)

**E\ Plate\ 8**: Space bar Stop Piece

**F\ Plate\ 8**: Space bar Bumpers

**G\ Plate\ 8**: Line Lock Locking Lever

**H\ Plate\ 8**: Retaining Plate Screws

**I\ Plate\ 8**: Nut (on the backspace rod)

**L\ Plate\ 8**: Back Space Key Lever Arm (to escapement)

**M\ Plate\ 8**: Back Space Key Lever (to key) (*undocumented*)

**N\ Plate\ 8**: Opening (on the end of shift lock)

**O\ Plate\ 8**: Line Lock Locking Shaft

**P\ Plate\ 8**: Prong (connects locking shaft with line lock rod)

**Q\ Plate\ 8**: Locking Shaft Collar

**R\ Plate\ 8**: Shift Lock Shaft

**S\ Plate\ 8**: Set Screw (for shift lock hub)

**T\ Plate\ 8**: Spring (for shift lock hub)

[]{#p9}

## Plate 9: Line Space Working Parts

![](plate_09.png)

**A\ Plate\ 9**: Set Screw (for variable line space drum brake pieces)

**B\ Plate\ 9**: Line Space Plunger Stop

**C\ Plate\ 9**: Screw (connects plunger to pawl)

**D\ Plate\ 9**: Plunger Spring

**E\ Plate\ 9**: Plunger Pawl

**F\ Plate\ 9**: Line Space Adjuster

**G\ Plate\ 9**: Line Space Plunger

**H\ Plate\ 9**: Ratchet Roll

**I\ Plate\ 9**: Ratchet Release Lever

**J\ Plate\ 9**: Ratchet Wheel

**K\ Plate\ 9**: Ratchet Arm

**L\ Plate\ 9**: Flat Spring (holds line space adjuster in one of the 3 positions)

**M\ Plate\ 9**: Tension Spring (pushes on the rathet)

**N\ Plate\ 9**: Pin (holds the tension spring)


[]{#p10}

## Plate 10: Front And Right View, Carriage Off

![](plate_10.png)

**A\ Plate\ 10**: Line Space Lever Pin

**B\ Plate\ 10**: Carriage Guide

**B~1~\ Plate\ 10**: Front Scale

**C\ Plate\ 10**: Screws (hold the fulcrum wire of keylinks) (*undocumented*)

**D\ Plate\ 10**: Fulcrum Pin Screws (fasten the spacebar shaft)

**E\ Plate\ 10**: Ribbon Pawls

**F\ Plate\ 10**: Carriage Lift Hooks

**G\ Plate\ 10**: Places Where Carriage Corners Rest

**H\ Plate\ 10**: Back Space Key Lever

**I\ Plate\ 10**: Tabular Key Lever

**J\ Plate\ 10**: Key Tops

**K\ Plate\ 10**: Lift Rail Spring Hanger And Bichrome Shift Lever Bracket

**L\ Plate\ 10**: Front Name Plate

**M\ Plate\ 10**: Margin Release Button

**N\ Plate\ 10**: Screw Shoulder of Lift Rail

**O\ Plate\ 10**: Lift Rail retainer spring

**P\ Plate\ 10**: Bell Trip Guard

**T\ Plate\ 10**: Right Margin Release


# GLOSSARY

**Bracket**: an overhanging member that projects from a structure (such as a wall) and is usually 
designed to support a vertical load or to strengthen an angle

**Collar**: something resembling a collar in shape or use (such as a ring or round flange to 
restrain motion or hold something in place)

**Dog**: part of a tool that prevents movement or imparts movement by
offering physical obstruction or engagement of some kind.

In the escapement, there is loose dog which usually holds the escapement wheel, and can rotate side
to side, and also move back/forward a bit, and the rigid dog, which can move only slightly back
and forward  and it pushes aside the loose dog, and temporarily holds the escapement wheel itself,
till the key is released and the loose dog catches the escapement wheel again (but on the next teeth).

**Fulcrum**: the support about which a lever turns

**Hanger**: ??

**Lug**: something (such as a handle) that projects like an ear

**Pawl**: a pivoted curved bar or lever whose free end engages with the teeth of a cogwheel or 
ratchet so that the wheel or ratchet can only turn or move one way.

**Plunger**: a piece with a motion like that of a ram or piston

**Ratchet**: a device consisting of a bar or wheel with a set of angled teeth in which a 
cog or tooth engages, allowing motion in one direction only.

**Roll**: sometimes an axis of rotation is called like that.

**Stud**: a small cylindrical piece that projects from a surface. Used to hook levers, springs, etc


**Wheel check**: similiar to a dog, this is something that prevents rotation of a wheel.


